<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 04.09.14
 * Time: 16:32
 */

namespace CMS\GeoBundle\Normalizer;

use CMS\GeoBundle\Response\GeoObjectIconResponse;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class GeoObjectIconNormalizer extends SerializerAwareNormalizer implements NormalizerInterface, DenormalizerInterface{

    /**
     * Denormalizes data back into an object of the given class
     *
     * @param mixed $data data to restore
     * @param string $class the expected class to instantiate
     * @param string $format format the given data was extracted from
     * @param array $context options available to the denormalizer
     *
     * @return object
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
        return $data;
    }

    /**
     * Checks whether the given class is supported for denormalization by this normalizer
     *
     * @param mixed $data Data to denormalize from.
     * @param string $type The class to which the data should be denormalized.
     * @param string $format The format being deserialized from.
     *
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        if (!is_array($data)){
            return false;
        }

        foreach (['id', 'geo_type', 'geometry', 'reference_id'] as $key) {
            if (!isset($data[$key])) {
                return false;
            }
        }

        return true;
    }

    /**
     * Normalizes an object into a set of arrays/scalars
     *
     * @param object $object object to normalize
     * @param string $format format the normalization result will be encoded as
     * @param array $context Context options for the normalizer
     *
     * @return array|scalar
     */
    public function normalize($object, $format = null, array $context = array())
    {
        $result = [];

        if(null !== ($path = $object->getPath())) {
            $result['path'] = $path;
        }

        if(null !== ($fillColor = $object->getFillColor())){
            $result['fillColor'] = $fillColor;
        }

        if(null !== ($fillOpacity = $object->getFillOpacity())){
            $result['fillOpacity'] = $fillOpacity;
        }

        if(null !== ($rotation = $object->getRotation())){
            $result['rotation'] = $rotation;
        }

        if(null !== ($scale = $object->getScale())){
            $result['scale'] = $scale;
        }

        if(null !== ($strokeColor = $object->getStrokeColor())){
            $result['strokeColor'] = $strokeColor;
        }

        if(null !== ($strokeOpacity = $object->getStrokeOpacity())){
            $result['strokeOpacity'] = $strokeOpacity;
        }

        if(null !== ($strokeWeight = $object->getStrokeWeight())){
            $result['strokeWeight'] = $strokeWeight;
        }

        return $result;
    }

    /**
     * Checks whether the given class is supported for normalization by this normalizer
     *
     * @param mixed $data Data to normalize.
     * @param string $format The format being (de-)serialized from or into.
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof GeoObjectIconResponse;
    }
}