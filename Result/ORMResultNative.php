<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 26.09.14
 * Time: 15:30
 */

namespace CMS\GeoBundle\Result;


use Doctrine\ORM\NativeQuery;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Traversable;

class ORMResultNative implements \Countable, \IteratorAggregate{

    private $query = null;

    private $limit = null;

    private $skip = null;

    private $result = null;

    private $count = null;

    public function __construct(NativeQuery $query, $limit = null, $skip = null){
        $this->query = $this->prepareQuery($query);

        if (null !== $limit && !is_numeric($limit)){
            throw new \Exception('Variable \'limit\' is not numeric');
        }

        if (null !== $skip && !is_numeric($skip)){
            throw new \Exception('Variable \'skip\' is not numeric');
        }

        $this->limit = $limit;
        $this->skip  = $skip;
    }

    public function getResult($hydrationMode = null){
        $this->result = $this->getQuery()->getResult($hydrationMode);

        return $this->result;
    }

    public function getSingleResult($hydrationMode = null){
        try{
            $this->result = $this->getQuery()->getSingleResult($hydrationMode);
        }
        catch(NoResultException $e){
            return null;
        }

        return $this->result;
    }

    public function getScalarResult(){
        try{
            $this->result = $this->query->getScalarResult();
        }
        catch(NoResultException $e){
            return null;
        }

        return $this->result;
    }

    public function getArrayResult(){
        $this->result = $this->getQuery()->getArrayResult();

        return $this->result;
    }

    public function getCountAll(){
        if (null === $this->count) {
            $query = $this->getQueryForCount();

            $connection = $query->getEntityManager()->getConnection();
            $sql = $query->getSQL();
            $parameters = $query->getParameters();

            $params = [];
            foreach ($parameters as $parameter) {
                $params = array_merge($params, [$parameter->getName() => $parameter->getValue()]);
            }

            $stmp = $connection->executeQuery($sql, $params);

            $this->count = (integer) $stmp->fetchColumn(0);
        }

        return $this->count;
    }

    private function getQuery(){
        $query = $this->cloneQuery($this->query);;
        $sql   = $query->getSQL();

        if (preg_match('/^\s?select/i', $sql)) {
            if (null !== $this->limit) {
                $sql .= " LIMIT :limit";
                $query->setParameter('limit', (int)$this->limit);
            }

            if (null !== $this->skip) {
                $sql .= " OFFSET :offset";
                $query->setParameter('offset', (int)$this->skip);
            }
        }

        return $query->setSQL($sql);
    }

    /**
     * @return NativeQuery
     */
    private function getQueryForCount(){
        $query = $this->cloneQuery($this->query);
        $sql   = $query->getSQL();

        if (preg_match('/^\s?select/i', $sql)) {
            $sql = preg_replace('/^\s?select\s+([\*\w\.\s0-9\:\-\,\+_\(\)]*)\s+from/i', 'SELECT count(*) as count FROM', $sql);
            $sql = preg_replace('/\s+order\s+by\s+.*([\s+dist]?)\s?/i', "", $sql);
        }

        return $query->setSQL($sql);
    }

    /**
     * @param NativeQuery $query
     * @return NativeQuery
     */
    private function cloneQuery(NativeQuery $query)
    {
        /* @var $cloneQuery Query */
        $cloneQuery = clone $query;

        $cloneQuery->setParameters(clone $query->getParameters());

        foreach ($query->getHints() as $name => $value) {
            $cloneQuery->setHint($name, $value);
        }

        return $cloneQuery;
    }

    private function appendTreeWalker(NativeQuery $query, $walkerClass)
    {
        $hints = $query->getHint(Query::HINT_CUSTOM_TREE_WALKERS);

        if ($hints === false) {
            $hints = array();
        }

        $hints[] = $walkerClass;
        $query->setHint(Query::HINT_CUSTOM_TREE_WALKERS, $hints);
    }

    private function prepareQuery(NativeQuery $query){
        $sql = $query->getSQL();

        if(preg_match('/offset\s+([0-9]+|[\:0-9a-z\-\_]+)/i', $sql, $matches)){
            if (preg_match('/:([0-9a-z\-\_])/i', $matches[1])){
                $this->skip = $query->getParameter($matches[1]);
            }
            else{
                $this->skip = $matches[1];
            }

            $sql = preg_replace('/offset\s+([0-9]+\s?|[\:0-9a-z\-\_]+)/i', "", $sql);
        }

        if(preg_match('/limit\s+([0-9]+|[\:0-9a-z\-\_]+)/i', $sql, $matches)){
            if (preg_match('/:([0-9a-z\-\_])/i', $matches[1])){
                $this->limit = $query->getParameter($matches[1]);
            }
            else{
                $this->limit = $matches[1];
            }

            $sql = preg_replace('/limit\s+([0-9]+\s?|[\:0-9a-z\-\_]+)/i', "", $sql);
        }

        $query->setSQL($sql);

        return $query;
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     */
    public function count()
    {
        if (null === $this->result){
            $this->getResult();
        }

        return count($this->result);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->getResult());
    }
}