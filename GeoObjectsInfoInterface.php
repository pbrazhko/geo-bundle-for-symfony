<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 06.10.14
 * Time: 15:09
 */

namespace CMS\GeoBundle;

use CMS\GeoBundle\Response\GeoObjectIconResponse;
use CMS\GeoBundle\Response\GeoObjectsInfoResponse;
use CMS\GeoBundle\Response\GeoObjectsListResponse;

interface GeoObjectsInfoInterface {

    /**
     * Return information of object
     *
     * @param $id
     * @return string
     */
    public function getGeoObjectsInfo($id);

    /**
     * Return list objects for this type
     *
     * @param string|null $query
     * @param int|null $limit
     * @param int|null $skip
     * @return GeoObjectsListResponse
     */
    public function getObjectsList($query = null, $limit = null, $skip = null);

    /**
     * Return icon for geo objects
     *
     * @return string|GeoObjectIconResponse
     */
    public function getIcon();

    /**
     * Return true is objects of type area
     *
     * @return boolean
     */
    public function isArea();

    /**
     * Return name type for this provider
     *
     * @return string
     */
    public static function getTypeName();
} 