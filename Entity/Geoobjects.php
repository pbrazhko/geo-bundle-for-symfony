<?php
namespace CMS\GeoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Geoobjects {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var Geometry
     */
    private $geometry;

    /**
     * @var integer
     */
    private $geotype;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $geometry
     * @return $this
     */
    public function setGeometry($geometry)
    {
        $this->geometry = $geometry;

        return $this;
    }

    /**
     * @return integer
     */
    public function getGeometry()
    {
        return $this->geometry;
    }

    /**
     * @param integer $geotype
     * @return $this
     */
    public function setGeotype($geotype)
    {
        $this->geotype = $geotype;

        return $this;
    }

    /**
     * @return integer
     */
    public function getGeotype()
    {
        return $this->geotype;
    }
} 