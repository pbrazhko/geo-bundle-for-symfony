<?php

namespace CMS\GeoBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * PointsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class GeometryRepository extends EntityRepository
{
    private $connection = null;


    private function getConnection(){
        if (null === $this->connection){
            $this->connection = $this->getEntityManager()->getConnection();
        }

        return $this->connection;
    }
}
