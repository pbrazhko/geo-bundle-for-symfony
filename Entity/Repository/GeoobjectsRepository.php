<?php
namespace CMS\GeoBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use CMS\GeoBundle\Entity\Geometry;
use CMS\GeoBundle\Entity\Geoobjects;
use CMS\GeoBundle\GeoObjectsInfoInterface;
use CMS\GeoBundle\Result\ORMResultNative;
use CMS\GeoBundle\Types\Point;
use CMS\GeoBundle\Types\GeometryInterface;

class GeoobjectsRepository extends EntityRepository
{
    private $connection;

    /**
     * Get geo objects by point
     *
     * @param Point $point
     * @param GeoObjectsInfoInterface $type
     * @param null $geometry_type
     * @return array
     */
    public function getByPoint(Point $point, GeoObjectsInfoInterface $type = null, $geometry_type = null)
    {
        $sql = "SELECT
                go.*,
                gm.id as geometry,
                AsText(gm.geom) as geom
              FROM
                cms_geoobjects go
                LEFT JOIN cms_geometry gm ON go.geometry = gm.id
              WHERE
                ST_Contains(gm.geom, GeomFromText(:point))";

        $parameters = [
            'point' => (string) $point
        ];

        if (null !== $type){
            $sql .= " AND go.geotype = :type";
            $parameters['type'] = $type->getTypeName();
        }

        if(null !== $geometry_type){
            $sql .= " AND GeometryType(gm.geom) = :geometry_type";
            $parameters['geometry_type'] = $geometry_type;
        }

        $sql .= " ORDER BY Area(gm.geom) DESC";

        $query = $this->getEntityManager()
            ->createNativeQuery($sql, $this->getRsmGeoobjects())
            ->setParameters($parameters);

        return new ORMResultNative($query);
    }

    /**
     * Get mapping for entity cms_geoobjects
     *
     * @return ResultSetMapping
     */
    private function getRsmGeoobjects()
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('CMS\GeoBundle\Entity\Geoobjects', 'go')
            ->addFieldResult('go', 'id', 'id')
            ->addFieldResult('go', 'geotype', 'geotype')
            ->addJoinedEntityResult('CMS\GeoBundle\Entity\Geometry', 'gm', 'go', 'geometry')
            ->addFieldResult('gm', 'geometry', 'id')
            ->addFieldResult('gm', 'geom', 'geom');

        return $rsm;
    }

    /**
     * Get all points in polygon. Polygon type is text.
     *
     * @param Geometry $object
     * @param GeoObjectsInfoInterface|null $type
     * @param $skip
     * @param $limit
     * @param null $geometry_type
     * @return array
     */
    public function getByGeometryObjectAsText(Geometry $object, GeoObjectsInfoInterface $type = null, $skip = null, $limit = null, $geometry_type = null)
    {
        $sql = "SELECT
                go.*,
                gm.id as geometry,
                AsText(gm.geom) as geom
              FROM
                cms_geoobjects as go
                LEFT JOIN cms_geometry gm ON go.geometry = gm.id
              WHERE
                ST_Contains(GeomFromText(:geometry), gm.geom) AND gm.id != :id";

        $parameters = array(
            'geometry' => ((string) $object->getGeom()),
            'id' => $object->getId()
        );

        if (null !== $type){
            $sql .= " AND go.geotype = :type";
            $parameters['type'] = $type->getTypeName();
        }

        if(null !== $geometry_type){
            $sql .= " AND GeometryType(gm.geom) = :geometry_type";
            $parameters['geometry_type'] = $geometry_type;
        }

        $query = $this->getEntityManager()
            ->createNativeQuery($sql, $this->getRsmGeoobjects())
            ->setParameters($parameters);

        return new ORMResultNative($query, $limit, $skip);
    }

    /**
     * Get connection
     *
     * @return \Doctrine\DBAL\Connection
     */
    private function getConnection()
    {
        return $this->connection?: $this->getEntityManager()->getConnection();
    }

    /**
     * Get all points in polygon. Polygon type is binary.
     *
     * @param Geometry $object
     * @param GeoObjectsInfoInterface|null $type
     * @param integer $skip
     * @param integer $limit
     * @param null $geometry_type
     * @throws \Doctrine\DBAL\DBALException
     * @return array
     */
    public function getByGeometryObjectAsBinary(Geometry $object, GeoObjectsInfoInterface $type = null, $skip = null, $limit = null, $geometry_type = null)
    {
        $conn = $this->getConnection();

        $countQuery = "SELECT
                 count(go.id) as count
               FROM
                 cms_geoobjects as go
                 LEFT JOIN cms_geometry gm ON go.geometry = gm.id
               WHERE
                 ST_Contains(GeomFromWKB(:geometry), gm.geom) AND go.id != :id";

        $countParameters = array(
            'geometry' => $object->getGeom(),
            'id' => $object->getId()
        );

        if (null !== $type){
            $countQuery .= " AND go.geotype = :type";
            $countParameters['type'] = $type->getTypeName();
        }

        if(null !== $geometry_type){
            $countQuery .= " AND GeometryType(gm.geom) = :geometry_type";
            $parameters['geometry_type'] = $geometry_type;
        }

        $cstmt = $conn->executeQuery($countQuery, $countParameters);

        $count = $cstmt->fetchColumn(0);

        $query = "SELECT
                go.*,
                gm.id as geometry,
                AsText(gm.geom) as geom
              FROM
                cms_geoobjects as go
                LEFT JOIN cms_geometry gm ON go.geometry = gm.id
              WHERE
                ST_Contains(GeomFromWKB(:geometry), gm.geom) AND go.id != :id";

        $parameters = array(
            'geometry' => ((string) $object->getGeom()),
            'id' => $object->getId()
        );

        if (null !== $type){
            $query .= " AND go.geotype = :type";
            $parameters['type'] = $type->getId();
        }

        if(null !== $geometry_type){
            $query .= " AND GeometryType(gm.geom) = :geometry_type";
            $parameters['geometry_type'] = $geometry_type;
        }

        if (null !== $limit){
            $query .= " limit :limit";
            $parameters['limit'] = $limit;
        }

        if (null !== $skip){
            $query .= " offset :offset";
            $parameters['offset'] = $skip;
        }

        $result = $this->getEntityManager()
            ->createNativeQuery($query, $this->getRsmGeoobjects())
            ->setParameters($parameters)
            ->getResult();

        return array(
            'count' => (int)$count,
            'data' => $result
        );
    }

    /**
     * Get polygon for country. Polygon as text.
     *
     * @param $id
     * @return mixed
     */
    public function getGeometryObjectAsText($id)
    {
        $sql = "SELECT gm.id, AsText(gm.geom) as geom FROM cms_geoobjects as go LEFT JOIN cms_geometry gm ON go.geometry = gm.id WHERE go.id = :id";

        $query = $this->getEntityManager()
            ->createNativeQuery($sql, $this->getRsmGeometry())
            ->setParameter('id', $id);

        return new ORMResultNative($query);
    }

    /**
     * Get mapping for entity geometry
     *
     * @return ResultSetMapping
     */
    private function getRsmGeometry()
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('CMS\GeoBundle\Entity\Geometry', 'gm');
        $rsm->addFieldResult('gm', 'id', 'id')
            ->addFieldResult('gm', 'geom', 'geom');

        return $rsm;
    }

    /**
     * @param GeoObjectsInfoInterface $type
     * @param int $limit
     * @param int $skip
     * @param null $geometry_type
     * @return ORMResultNative
     */
    public function getAll(GeoObjectsInfoInterface $type = null, $limit = null, $skip = null, $geometry_type = null){
        $sql = "SELECT
                go.*,
                gm.id as geometry,
                AsText(gm.geom) as geom
              FROM
                cms_geoobjects go
                LEFT JOIN cms_geometry gm ON go.geometry = gm.id
              WHERE 1=1";

        $parameters = [];

        if (null !== $type){
            $sql .= " AND go.geotype = :type";
            $parameters['type'] = $type->getTypeName();
        }

        if(null !== $geometry_type){
            $sql .= " AND GeometryType(gm.geom) = :geometry_type";
            $parameters['geometry_type'] = $geometry_type;
        }

        $query = $this->getEntityManager()
            ->createNativeQuery($sql, $this->getRsmGeoobjects())
            ->setParameters($parameters);

        return new ORMResultNative($query, $limit, $skip);
    }

    /**
     * Get polygon for country. Polygon as binary.
     *
     * @param $id
     * @return mixed
     */
    public function getGeometryObjectAsBinary($id)
    {
        $sql = "SELECT gm.id, AsBinary(gm.geom) as geom FROM cms_geoobjects as go LEFT JOIN cms_geometry gm ON go.geometry = gm.id WHERE go.id = :id";

        $query = $this->getEntityManager()
            ->createNativeQuery($sql, $this->getRsmGeometry())
            ->setParameter('id', $id);

        return new ORMResultNative($query);
    }

    /**
     * Create polygon (circle) by center (point) and radius.
     * Return polygon as binary.
     *
     * @param Point $point
     * @param $distance
     * @return bool|string
     */
    public function getCircleAsBinary(Point $point, $distance)
    {
        $q = "SELECT :system_id as id, AsBinary(Buffer(GeomFromText('" . ((string)$point) . "'), :distance)) as geom";

        $result = $this->getEntityManager()
            ->createNativeQuery($q, $this->getRsmGeometry())
            ->setParameter('distance', $distance)
            ->setParameter('system_id', md5($distance))
            ->getResult();

        return reset($result);
    }

    /**
     * Create polygon (circle) by center (point) and radius.
     * Return polygon as text.
     *
     * @param Point $point
     * @param $distance
     * @return bool|string
     */
    public function getCircleAsText(Point $point, $distance)
    {
        $sql = "SELECT :system_id as id,AsText(Buffer(GeomFromText('" . ((string)$point) . "'), :distance)) as geom";

        $query = $this->getEntityManager()
            ->createNativeQuery($sql, $this->getRsmGeometry())
            ->useResultCache(false)
            ->setParameter('distance', $distance, 'float')
            ->setParameter('system_id', md5($distance));

        return new ORMResultNative($query);
    }

    /**
     * Ad new geometry object
     *
     * @param GeometryInterface $geometry
     * @param GeoObjectsInfoInterface $type
     * @return bool
     */
    public function addGeoobject(GeometryInterface $geometry, GeoObjectsInfoInterface $type)
    {
        $conn = $this->getConnection();

        $conn->executeUpdate("INSERT INTO cms_geometry (`geom`) VALUES(GeomFromText('" . ((string)$geometry) . "'))");

        $geometry = $conn->lastInsertId();

        $conn->insert('cms_geoobjects',
            array(
                'geometry' => $geometry,
                'geotype' => $type->getTypeName()
            )
        );

        return $conn->lastInsertId();
    }

    /**
     * @param $id
     * @param GeometryInterface $geometry
     * @param GeoObjectsInfoInterface $type
     * @return \Doctrine\DBAL\Driver\Statement
     */
    public function updateGeoobject($id, GeometryInterface $geometry, GeoObjectsInfoInterface $type = null)
    {
        $conn = $this->getConnection();

        if (null !== $type) {
            $query = "UPDATE cms_geoobjects SET ";
            $set       = array();
            $variables = array();

            if (null !== $type){
                $set['geotype'] = ':geotype';
                $variables['geotype'] = $type->getTypeName();
            }

            foreach($set as $field=>$param){
                $query .= $field . '=' . $param . ', ';
            }

            $query = substr($query, 0, -2) . " WHERE id = :id";
            $variables['id'] = $id;

            $conn->executeUpdate($query, $variables);
        }

        $query = "UPDATE
                    cms_geoobjects go
                    INNER JOIN cms_geometry gm ON go.geometry = gm.id
                  SET
                    gm.geom = GeomFromText(\"" . ((string) $geometry) .  "\")
                  WHERE go.id = " . $id;

        return $conn->executeUpdate($query);
    }

    /**
     * Get geometry object by reference id
     *
     * @param $id
     * @param GeoObjectsInfoInterface $type
     * @return ORMResultNative
     */
    public function getByReference($id, GeoObjectsInfoInterface $type = null)
    {
        $sql = "SELECT
                go.*,
                gm.id as geometry,
                AsText(gm.geom) as geom
              FROM
                cms_geoobjects as go
                LEFT JOIN cms_geometry gm ON go.geometry = gm.id
              WHERE
                1=1";

        $parameters = [];

        if (is_array($id)){
            $sql .= " AND go.reference_id IN('" . implode("', '", $id) ."')";
        }
        else{
            $sql .= " AND go.reference_id = :id";
            $parameters['id'] = $id;
        }

        if (null !== $type){
            $sql .= " AND go.geotype = :type";
            $parameters['type'] = $type->getTypeName();
        }

        $query = $this->getEntityManager()
            ->createNativeQuery($sql, $this->getRsmGeoobjects())
            ->setParameters($parameters);

        return new ORMResultNative($query);
    }

    /**
     * Get geometry object by id
     *
     * @param $id
     * @return ORMResultNative
     */
    public function getById($id)
    {
        $sql = "SELECT
                go.*,
                gm.id as geometry,
                AsText(gm.geom) as geom
              FROM
                cms_geoobjects as go
                LEFT JOIN cms_geometry gm ON go.geometry = gm.id
              WHERE
                go.id = :id";

        $query = $this->getEntityManager()
            ->createNativeQuery($sql, $this->getRsmGeoobjects())
            ->setParameter('id', $id);

        return new ORMResultNative($query);
    }

    /**
     * Get objects by min distance of point and type
     *
     * @param Point $point Start point
     * @param GeoObjectsInfoInterface $type Type searching point
     * @return ORMResultNative
     */
    public function getByMinDistanceOfType(Point $point, GeoObjectsInfoInterface $type)
    {
        $sql = "SELECT
                go.*,
                gm.id as geometry,
                AsText(gm.geom) as geom,
                ST_Distance(GeomFromText(:point), gm.geom) as distance
              FROM
                cms_geoobjects as go
                LEFT JOIN cms_geometry gm ON go.geometry = gm.id
              WHERE
                gm.geom != GeomFromText(:point) AND
                go.geotype = :type
               ORDER BY distance
               LIMIT 1
              ";

        $query = $this->getEntityManager()
            ->createNativeQuery($sql, $this->getRsmGeoobjects())
            ->setParameter('point', (string)$point)
            ->setParameter('type', $type->getTypeName());

        return new ORMResultNative($query);
    }

    public function removeGeoobject(Geoobjects $geoobject)
    {
        $conn = $this->getConnection();

        $conn->executeUpdate('DELETE FROM cms_geometry WHERE id = :id', array('id' => $geoobject->getGeometry()->getId()));
        $conn->executeUpdate('DELETE FROM cms_geoobjects WHERE id = :id', array('id' => $geoobject->getId()));

        return true;
    }
} 