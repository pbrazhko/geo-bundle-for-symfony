<?php

namespace CMS\GeoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Points
 */
class Geometry
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var Geoobjects
     */
    private $geoobject;

    /**
     * @var string
     */
    private $geometry_type;

    /**
     * @var string
     */
    private $geom;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Geoobjects
     */
    public function getGeoobject()
    {
        return $this->geoobject;
    }

    /**
     * @param Geoobjects $geoobject
     * @return $this
     */
    public function setGeoobject($geoobject)
    {
        $this->geoobject = $geoobject;

        return $this;
    }

    /**
     * @return string
     */
    public function getGeometryType()
    {
        return $this->geometry_type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setGeometryType($type)
    {
        $this->geometry_type = $type;

        return $this;
    }

    /**
     * @param string $geom
     * @return $this
     */
    public function setGeom($geom)
    {
        $this->geom = $geom;

        return $this;
    }

    /**
     * @return string
     */
    public function getGeom()
    {
        return $this->geom;
    }
}
