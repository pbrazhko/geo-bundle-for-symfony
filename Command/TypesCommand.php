<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 09.09.14
 * Time: 15:37
 */

namespace CMS\GeoBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TypesCommand extends ContainerAwareCommand{
    public function configure(){
        $this->setName('cms:geo:get-types');
        $this->setDescription('Return all geo objects types');
    }

    public function execute(InputInterface $input, OutputInterface $output){
        $geoObjectsProviderService = $this->getContainer()->get('geo.info.providers');

        $types = $geoObjectsProviderService->getAll();

        if (count($types) <= 0){
            $output->writeln("<info>Types not found.</info>");
            return false;
        }

        foreach($types as $name=>$provider){
            $output->writeln("<info>Type: " . $name . "\nClass: " . get_class($provider) . "\n</info>");
        }
    }
} 