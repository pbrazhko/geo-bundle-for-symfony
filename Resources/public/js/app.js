'use strict';

angular.module('GeoObjectsBundle', [
        'GeoObjectsBundle.controllers',
        'GeoObjectsBundle.services.GeoObjectsFilter',
        'GeoObjectsBundle.services.GeoObjectsStore',
        'GeoObjectsBundle.services.GeoObjectsTypesStore',
        'GeoObjectsBundle.services.paginator',
        'GeoObjectsBundle.factories',
        'GeoObjectsBundle.directives.map',
        'GeoObjectsBundle.directives.paginator',
        'GeoObjectsBundle.filters',
        'ngRoute'
    ])
    .config([
        '$interpolateProvider',
        '$routeProvider',
        '$httpProvider',
        function($interpolateProvider, $routeProvider, $httpProvider){
            $interpolateProvider.startSymbol('{[').endSymbol(']}');

            $routeProvider
                .when('/list', {
                    templateUrl: '../bundles/geo/html/list.html',
                    controller: 'GeoObjects'
                })
                .when('/edit/:type/:reference?', {
                    templateUrl: '../bundles/geo/html/edit.html',
                    controller: 'GeoObjectsEdit'
                })
                .otherwise({
                    redirectTo: '/list'
                });

            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

            // Переопределяем дефолтный transformRequest в $http-сервисе
            $httpProvider.defaults.transformRequest = [function(data)
            {
                /**
                 * рабочая лошадка; преобразует объект в x-www-form-urlencoded строку.
                 * @param {Object} obj
                 * @return {String}
                 */
                var param = function(obj)
                {
                    var query = '';
                    var name, value, fullSubName, subValue, innerObj, i;

                    for(name in obj)
                    {
                        value = obj[name];

                        if(value instanceof Array)
                        {
                            for(i=0; i<value.length; ++i)
                            {
                                subValue = value[i];
                                fullSubName = name + '[' + i + ']';
                                innerObj = {};
                                innerObj[fullSubName] = subValue;
                                query += param(innerObj) + '&';
                            }
                        }
                        else if(value instanceof Object)
                        {
                            for(var subName in value)
                            {
                                subValue = value[subName];
                                fullSubName = name + '[' + subName + ']';
                                innerObj = {};
                                innerObj[fullSubName] = subValue;
                                query += param(innerObj) + '&';
                            }
                        }
                        else if(value !== undefined && value !== null)
                        {
                            query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
                        }
                    }

                    return query.length ? query.substr(0, query.length - 1) : query;
                };

                return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
            }];
        }
    ])

