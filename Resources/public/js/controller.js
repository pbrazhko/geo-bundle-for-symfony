'use strict';

angular.module('GeoObjectsBundle.controllers',[])
    .controller('GeoObjects', function ($scope, GeoObjectsFilter, translator, GeoObjectsStore, GeoObjectsTypesStore, paginator, $filter){
        $scope.trans = translator.trans;

        $scope.setFilter = function (name, value){
            GeoObjectsFilter.offsetSet(name, value);
        };

        $scope.setContent = function (response){
            $scope.content = response.data.data;
            paginator.count = response.data.count;
        };

        $scope.getUrl = function (route, params){
            return Routing.generate(route, params);
        };

        $scope.$watch('filter.type', function(newValue, oldValue){
            if(newValue != oldValue){
                paginator.setPage(0);
                GeoObjectsStore.get(GeoObjectsFilter.get(), paginator.limit, paginator.limit*paginator.page).then($scope.setContent);
            }
        });

        $scope.remove = function (id){
            GeoObjectsStore.remove(id).success(function(data){
                GeoObjectsStore.get(GeoObjectsFilter.get(), paginator.limit, paginator.limit*paginator.page).then($scope.setContent);
            });
        };

        $scope.$watch('content', function (newValue, oldValue){
            if (newValue != oldValue && $scope.types != undefined){
                for(var i in $scope.content){
                    var type = $filter('filter')($scope.types, function(element){
                        if (element.id == $scope.content[i].type){
                            return element;
                        }
                    });

                    if (type.length > 0) {
                        $scope.content[i].type = type[0].name;
                    }
                }
            }
        }, true);

        $scope.$watch('paginator.page', function(newValue, oldValue) {
            if (newValue != oldValue) {
                GeoObjectsStore.get(GeoObjectsFilter.get(), paginator.limit, paginator.limit*paginator.page).then($scope.setContent);
            }
        });

        GeoObjectsStore.get(GeoObjectsFilter.get(), paginator.limit, paginator.limit*paginator.page).then($scope.setContent);

        GeoObjectsTypesStore.get().success(function (data) {
            $scope.types = data;
        })
        .error(function (data){
            $scope.error = data.message;
        });
    })
    .controller('GeoObjectsEdit', function($scope, GeoObjectsStore, GeoObjectsTypesStore, $routeParams, $window, $location, converterGeometry){
        $scope.modeEdit = false;
        $scope.type = {};

        $scope.closeError = function (){
            $scope.error = null;
        }

        $scope.cancel = function(){
            if ($scope.isBlank){
                $window.close();
            }

            $location.path('#/list');
        };

        $scope.save = function () {
            var geometries = converterGeometry.fromGoogle($scope.$root.geometries);

            if (Object.keys(geometries).length != 1) {
                $scope.error = 'Type geometry not one';
                return false;
            }

            if( $scope.geoobject == undefined){
                $scope.error = 'Type or reference id is not defined';
                return false;
            }

            var geometry_type = Object.keys(geometries)[0];

            if (geometry_type != 'polygon' && geometries[geometry_type].length > 1){
                $scope.error = 'Geometry is not one! Allowed one element.';
                return false;
            }

            var data = {
                'geometry': geometry_type == 'point' ? geometries[geometry_type][0] : geometries[geometry_type],
                'geometry_type': geometry_type,
                'type': $scope.geoobject.type,
                'reference_id': $scope.geoobject.reference
            };

            if ($scope.modeEdit) {
                data['id'] = $scope.geoobject.id;
            }

            if (geometry_type == 'polygon' && geometries[geometry_type].length > 1) {
                data.geometry_type = 'multipolygon';
            }

            GeoObjectsStore.save(data).success(function () {
                if ($scope.isBlank){
                    $window.close();
                }

                $location.path('#/list');
            })
            .error(function(data){
                $scope.error = data.message;
            });
        }

        $scope.loadObject = function (type, reference){
            GeoObjectsStore.getByReference(reference, type)
                .success(function (data) {
                    var result = data.data;

                    if (data.count != undefined && data.count > 0) {
                        $scope.modeEdit = true;
                        $scope.geoobject = result[0];
                        $scope.$broadcast('map.drawGeometry', $scope.geoobject);
                    }
                    else{
                        $scope.geoobject = {
                            'type': type,
                            'reference': reference
                        }
                    }

                    $scope.getType(type);
                })
                .error(function (data){
                    $scope.error = data.message;
                });
        };

        $scope.getType = function (type){
            GeoObjectsTypesStore.getByName(type)
                .success(function(data){
                    var mode = data.isArea == true?'POLYGON':'MARKER';

                    $scope.$broadcast('map.setDrawingControlMode', mode);
                })
                .error(function (data){
                    $scope.error = data.message;
                })
        };

        if ($routeParams.reference != undefined && $routeParams.type != undefined) {
            $scope.loadObject($routeParams.type, $routeParams.reference);
        }
    })