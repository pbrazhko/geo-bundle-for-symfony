/**
 * Created by pavel on 28.07.14.
 */

'use strict';

angular.module('GeoObjectsBundle.factories',[])
    .factory('translator', function (){
        return {
            trans: function (name, domain){
                return Translator.trans(name,{},domain);
            }
        }
    })
    .factory('converterGeometry', function (){
        return {
            'fromGoogle': function (geometriesOnMap){
                var geometries = {};

                if (typeof (geometriesOnMap) == "object") {
                    for (var type in geometriesOnMap) {
                        var g = geometriesOnMap[type];

                        if (g.length <= 0) continue;

                        for (var i = 0; i < g.length; i++) {
                            if (g[i] == undefined) continue;

                            switch (type) {
                                case 'marker':
                                    geometries['point'] = geometries['point'] || new Array();

                                    geometries['point'].push([g[i].getPosition().lat(), g[i].getPosition().lng()]);
                                    break;
                                case 'polygon':
                                    geometries['polygon'] = geometries['polygon'] || new Array();
                                    geometries['polygon'][i] = new Array();

                                    var path = g[i].getPath();

                                    for (var s = 0; s < path.length; s++) {
                                        geometries['polygon'][i].push([path.getAt(s).lat(), path.getAt(s).lng()]);
                                    }
                                    break;
                                case 'polyline':
                                    geometries['linestring'] = geometries['linestring'] || new Array();
                                    geometries['linestring'][i] = new Array();

                                    var path = g[i].getPath();

                                    for (var s = 0; s < path.length; s++) {
                                        geometries['linestring'][i].push([path.getAt(s).lat(), path.getAt(s).lng()]);
                                    }
                                    break;
                                case 'rectangle':
                                    geometries['polygon'] = geometries['polygon'] || new Array();
                                    geometries['polygon'][i] = new Array();

                                    var bounds = g[i].getBounds();

                                    geometries['polygon'][i].push([bounds.getNorthEast().lat(), bounds.getNorthEast().lng()]);
                                    geometries['polygon'][i].push([bounds.getNorthEast().lat(), bounds.getSouthWest().lng()]);
                                    geometries['polygon'][i].push([bounds.getSouthWest().lat(), bounds.getSouthWest().lng()]);
                                    geometries['polygon'][i].push([bounds.getSouthWest().lat(), bounds.getNorthEast().lng()]);
                                    geometries['polygon'][i].push([bounds.getNorthEast().lat(), bounds.getNorthEast().lng()]);

                                    break;
                            }
                        }
                    }
                }

                return geometries;
            }
        }
    });