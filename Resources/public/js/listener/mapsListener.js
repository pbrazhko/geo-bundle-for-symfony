/**
 * Created by pavel on 20.11.15.
 */

(function ($) {
        var mapsListener = {
            onDynamicMapInit: function (event, map, token) {
                console.log('init map ' + token);
            },
            onAddGeometry: function (event, type, geometry) {
                console.log('add geometry type: ' + type);
            },
            onUpdateGeometry: function (event, type, geometry) {
                console.log('update geometry type' + type);
            },
            onDeleteGeometry: function (event, type, geometry) {
            },
            onGeometryClick: function (event, geometry, i, type, token) {
                if (type == google.maps.drawing.OverlayType.MARKER) {
                    console.log('geometry click')
                }
            }
        };

    $(window).on('map.init.dynamic_map', mapsListener.onDynamicMapInit);
        $(window).on('map.add_geometry', mapsListener.onAddGeometry);
        $(window).on('map.update_geometry', mapsListener.onUpdateGeometry);
})(jQuery);