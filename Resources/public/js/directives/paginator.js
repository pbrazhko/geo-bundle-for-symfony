/**
 * Created by pavel on 20.10.14.
 */

'use strict';

angular.module('GeoObjectsBundle.directives.paginator', [])
    .directive('paginator', function factory($location) {
        return {
            replace: true,
            restrict: 'E',
            controller: function ($scope, paginator) {
                $scope.paginator = paginator;
                $scope.path = $location.path();
            },
            template: '<div class="span6 offset0 text-left">' +
            '<div class="pagination pagination-small" style="margin: 0" ng-show="paginator.countPage() > 1">' +
            '<ul>' +
            '<li ng-class="paginator.isFirstPage() && \'disabled\'"><a href="#{[path]}" ng-click="paginator.firstPage()">Start</a></li>' +
            '<li ng-class="paginator.page==page && \'active\'"ng-repeat="page in []|forLoop:this" ><a href="#{[path]}" ng-click="paginator.setPage(page)">{[page+1]}</a></li>' +
            '<li ng-class="paginator.isEndPage() && \'disabled\'"><a href="#{[path]}" ng-click="paginator.endPage()">End</a></li>' +
            '</ul>' +
            '</div></div>'
        };
    })
