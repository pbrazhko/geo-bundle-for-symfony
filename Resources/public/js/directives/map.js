/**
 * Created by pavel on 20.10.14.
 */

'use strict';

angular.module('GeoObjectsBundle.directives.map', [])
    .directive('map', [function(){
        return {
            restrict: 'E',
            scope: {
                parametersMap: '&mapOptions',
                parametersDrawing: '&drawingOptions'
            },
            controller: function ($scope){
                $scope.$root.geometries = {
                    'polygon': new Array(),
                    'marker': new Array(),
                    'circle': new Array(),
                    'polyline': new Array(),
                    'rectangle': new Array()
                };

                $scope.defaultParametersMap = {
                    center:{
                        lat: 0,
                        lon: 0
                    },
                    zoom: 6,
                    bgcolor: '#ffffff',
                    disableDoubleClickZoom: false,
                    draggable: false,
                    scrollwheel: false,
                    disableDefaultUI: false,
                    mapType: 'ROADMAP',
                    drawingManager: false,
                    width: 'auto',
                    height: 'auto'
                };

                $scope.defaultParametersDrawing = {
                    drawing: false,
                    modes: []
                };

                $scope.defaultParametersMarker = {
                    position: {
                        lat: 0,
                        lon: 0
                    },
                    icon: '',
                    title: ''
                };

                $scope.geometriesOptions = function (){
                    return {
                        fillColor: '#cccccc',
                        fillOpacity: 0.5,
                        strokeWeight: 5,
                        clickable: true,
                        draggable: true,
                        editable: true
                    }
                };

                $scope.prepareOptions = function (params, iAttr){
                    if (iAttr == undefined) return params;

                    for (var attr in params) {
                        if (attr === 'mapTypeId') continue;

                        if (attr === 'center' || attr === 'position') {
                            if (iAttr.lat === undefined || iAttr.lon === undefined) {
                                console.error('Lat and lon is required attributes for directive \'Map\'');
                                params[attr] = new google.maps.LatLng(params[attr].lat, params[attr].lon);
                                continue;
                            }

                            params[attr] = new google.maps.LatLng(iAttr.lat, iAttr.lon);
                            continue;
                        }

                        if (typeof(params[attr]) === 'object') {
                            params[attr] = $scope.prepareOptions(params[attr], iAttr);
                            continue;
                        }

                        if (iAttr[attr.toLocaleLowerCase()] != undefined && iAttr[attr.toLocaleLowerCase()] != '') {
                            params[attr] = iAttr[attr.toLocaleLowerCase()], typeof(params[attr]);
                        }
                    }
                    return params;
                }

                $scope.addGeomerty = function (id, geometry, type, callback){
                    if ($scope.$root.geometries[type] == undefined) return false;

                    $scope.$root.geometries[type].push(geometry);

                    var mapCenter = null;
                    if (type == 'marker'){
                        mapCenter = geometry.getPosition();
                    }
                    else if(type == 'polygon'){
                        mapCenter = geometry.getPath().getAt(0);
                    }
                    else if(type == 'circle'){
                        mapCenter = geometry.getCenter();
                    }
                    else if(type == 'polyline'){
                        mapCenter = geometry.getPath().getAt(0);
                    }

                    $scope.map.setCenter(mapCenter);
                    var menu = new $scope.menu();

                    google.maps.event.addListener(geometry, 'rightclick', function(event){
                        menu.open($scope.map, this, id, event.vertex);
                    });

                    if (typeof(callback) == 'object'){
                        if (callback.event != undefined && callback.fn != undefined){
                            google.maps.event.addListener(geometry, callback.event, function (event){
                                callback.fn(event, this, id);
                            });
                        }
                    }

                    return true;
                };

                $scope.deleteGeometry = function(geometry, id){
                    var geometries = new Array();

                    if (geometry instanceof google.maps.Polygon){
                        geometries = $scope.$root.geometries.polygon;
                    }
                    else if(geometry instanceof google.maps.Marker){
                        geometries = $scope.$root.geometries.marker
                    }
                    else if(geometry instanceof google.maps.google.maps.Circle){
                        geometries = $scope.$root.geometries.circle;
                    }
                    else if(geometry instanceof google.maps.Rectangle){
                        geometries = $scope.$root.geometries.rectangle;
                    }
                    else if(geometry instanceof google.maps.Polyline){
                        geometries = $scope.$root.geometries.polyline;
                    }

                    for(var i = 0; i < geometries.length; i++){
                        if( geometries[i] == geometry){
                            geometries[i].setMap(null);
                            geometries.splice(i, 1);
                            break;
                        }
                    }

                    $scope.$emit('map.deleteGeometry', geometry, id);

                    return true;
                };

                $scope.getWidth = function (){
                    var width = Math.max(
                        document.body.scrollWidth, document.documentElement.scrollWidth,
                        document.body.offsetWidth, document.documentElement.offsetWidth,
                        document.body.clientWidth, document.documentElement.clientWidth
                    );

                    return width/100*98;
                };

                $scope.getHeight = function (){
                    var height = Math.max(
                        document.body.scrollHeight, document.documentElement.scrollHeight,
                        document.body.offsetHeight, document.documentElement.offsetHeight,
                        document.body.clientHeight, document.documentElement.clientHeight
                    );

                    return height/100*80;
                },

                $scope.menu = function(){
                    this.div_ = document.createElement('div');
                    this.div_.className = 'gm-cnt-menu-item';
                    this.div_.innerHTML = 'Delete';

                    var menu = this;
                    google.maps.event.addDomListener(this.div_, 'click', function(event) {
                        $scope.deleteGeometry(menu.get('geometry'), menu.get('id'));
                        menu.close();
                    });
                };

                $scope.menu.prototype = new google.maps.OverlayView();

                $scope.menu.prototype.open = function (map, geometry, id, vertex){
                    if (geometry instanceof google.maps.Polygon){
                        this.set('position', geometry.getPath().getAt(vertex!=undefined?vertex:0));
                    }
                    else if(geometry instanceof google.maps.Marker){
                        this.set('position', geometry.getPosition());
                    }
                    else if(geometry instanceof google.maps.google.maps.Circle){
                        this.set('position', geometry.getCenter());
                    }
                    else if(geometry instanceof google.maps.Rectangle){
                        this.set('position', geometry.getBounds().getCenter());
                    }
                    else if(geometry instanceof google.maps.Polyline){
                        this.set('position', geometry.getPath().getAt(vertex!=undefined?vertex:0));
                    }
                    else{
                        return false;
                    }

                    this.set('geometry', geometry);
                    this.set('id', id);
                    this.setMap(map);
                    this.draw();
                };

                $scope.menu.prototype.draw = function () {
                    var position = this.get('position');

                    var projection = this.getProjection();

                    if (!position || !projection) {
                        return;
                    }

                    var point = projection.fromLatLngToDivPixel(position);
                    this.div_.style.top = point.y + 'px';
                    this.div_.style.left = point.x + 'px';
                };

                $scope.menu.prototype.onAdd = function() {
                    var menu = this;
                    var map = this.getMap();
                    this.getPanes().floatPane.appendChild(this.div_);

                    this.divListener_ = google.maps.event.addDomListener(map.getDiv(), 'mousedown', function(e) {
                        if (e.target != menu.div_) {
                            menu.close();
                        }
                    }, true);
                };

                $scope.menu.prototype.close = function() {
                    this.setMap(null);
                };

                $scope.menu.prototype.onRemove = function() {
                    google.maps.event.removeListener(this.divListener_);
                    this.div_.parentNode.removeChild(this.div_);

                    // clean up
                    this.set('position');
                    this.set('path');
                    this.set('vertex');
                };

                $scope.initMap = function (element){
                    var el = document.createElement('div');

                    element.prepend(el);

                    el.style.width = ($scope.parametersMap.width!='auto'?$scope.parametersMap.width:$scope.getWidth()) + 'px';
                    el.style.height = ($scope.parametersMap.height!='auto'?$scope.parametersMap.height:$scope.getHeight()) + 'px';

                    $scope.map = new google.maps.Map(el, $scope.parametersMap);

                    if ($scope.parametersDrawing.drawing == true){
                        $scope.drawingManager = new google.maps.drawing.DrawingManager();

                        var modes = new Array();
                        if ($scope.parametersDrawing.modes.length > 0) {
                            for (var i = 0; i < $scope.parametersDrawing.modes.length; i++) {
                                modes.push(google.maps.drawing.OverlayType[$scope.parametersDrawing.modes[i]]);
                            }
                        }

                        var drawingOptions = {
                            drawingControl: true,
                            drawingControlOptions: {
                                position: google.maps.ControlPosition.TOP_CENTER,
                                drawingModes: modes
                            },
                            circleOptions: $scope.geometriesOptions(),
                            polygonOptions: $scope.geometriesOptions(),
                            polylineOptions: $scope.geometriesOptions(),
                            rectangleOptions: $scope.geometriesOptions(),
                            markerOptions: $scope.geometriesOptions()
                        };

                        $scope.drawingManager.setOptions(drawingOptions);
                        $scope.drawingManager.setMap($scope.map);

                        google.maps.event.addListener($scope.drawingManager, 'overlaycomplete', function(event){
                            return $scope.addGeomerty(null, event.overlay, event.type);
                        });
                    }

                    google.maps.event.addListener($scope.map, "rightclick", function(event) {
                        var lat = event.latLng.lat();
                        var lng = event.latLng.lng();

                        $scope.$emit('map.rightclick', lat, lng);
                    });

                    $scope.$emit('map.init', $scope.map.getCenter());

                    return el;
                };

                $scope.$on('map.resize', function (){
                    var center = $scope.map.getCenter();
                    google.maps.event.trigger($scope.map, "resize");
                    $scope.map.setCenter(center);
                });

                $scope.$on('map.drawGeometry', function (event, object, callback, icon){
                    var geometry = object.geometry;
                    var type = geometry.geometry_type;

                    if (type == 'POINT') {
                        var gm = new google.maps.Marker($scope.geometriesOptions());

                        gm.setPosition(new google.maps.LatLng(geometry.geom[0], geometry.geom[1]))
                        gm.setMap($scope.map);

                        if (icon !== undefined) {
                            if (typeof(icon.path) != 'number'){
                                icon.path = google.maps.SymbolPath[icon.path];
                            }
                            gm.setIcon(icon);

                        }

                        $scope.addGeomerty(object.id, gm, 'marker', callback);
                    }
                    else if(type == 'POLYGON'){
                        var gm = new google.maps.Polygon($scope.geometriesOptions());
                        var path = new Array();
                        var coords = geometry.geom[0];

                        if (coords.length != undefined) {
                            for (var i = 0; i < coords.length; i++){
                                path.push(new google.maps.LatLng(coords[i][0],coords[i][1]))
                            }
                        }

                        if (path.length <= 0){
                            console.info('Path form polygon is not valid. Cancel!');
                            return false;
                        }

                        gm.setPath(path);
                        gm.setMap($scope.map);

                        $scope.addGeomerty(object.id, gm, 'polygon', callback);
                    }
                    else if(type == 'MULTIPOLYGON') {
                        var polygons = geometry.geom;

                        for (var s = 0; s < polygons.length; s++){
                            var gm = new google.maps.Polygon($scope.geometriesOptions());
                            var path = new Array();
                            var coords = polygons[s];

                            if (coords.length != undefined) {
                                for (var i = 0; i < coords.length; i++) {
                                    path.push(new google.maps.LatLng(coords[i][1], coords[i][0]))
                                }
                            }

                            if (path.length <= 0) {
                                console.info('Path form polygon is not valid. Cancel!');
                                return false;
                            }

                            gm.setPath(path);
                            gm.setMap($scope.map);

                            $scope.addGeomerty(object.id, gm, 'polygon', callback);
                        }

                        return true;
                    }
                    else{
                        console.info('Type geometry is not supported. Cancel!');
                        return false;
                    }
                });

                $scope.$on('map.setDrawingControlMode', function (event, mode){
                    var drawingOptions = {
                        drawingControl: true,
                        drawingControlOptions: {
                            position: google.maps.ControlPosition.TOP_CENTER,
                            drawingModes: [google.maps.drawing.OverlayType[mode]]
                        },
                        circleOptions: $scope.geometriesOptions(),
                        polygonOptions: $scope.geometriesOptions(),
                        polylineOptions: $scope.geometriesOptions(),
                        rectangleOptions: $scope.geometriesOptions(),
                        markerOptions: $scope.geometriesOptions()
                    };

                    if ($scope.drawingManager){
                        $scope.drawingManager.setOptions(drawingOptions);
                    }
                })
            },
            link:function($scope, element, iAttr){
                $scope.parametersMap = $scope.prepareOptions($scope.defaultParametersMap, $scope.parametersMap());
                $scope.parametersDrawing = $scope.prepareOptions($scope.defaultParametersDrawing, $scope.parametersDrawing());

                var el =  $scope.initMap(element);
                element.replaceWith(el);
            }
        }
    }]);