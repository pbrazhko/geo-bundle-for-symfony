/**
 * Created by pavel on 23.07.14.
 */

'use strict';

angular.module('GeoObjectsBundle.filters',[])
    .filter('forLoop', [function() {
        return function(input, $scope) {
            var start, end, interval = 10;

            start = $scope.paginator.page - interval/2;
            end = $scope.paginator.page + interval/2;

            if ((interval/2-$scope.paginator.page) > 0){
                end = end + (interval/2-$scope.paginator.page);
            }

            if(end > $scope.paginator.countPage()){
                end = $scope.paginator.countPage();
            }

            if (((interval/2+$scope.paginator.page) - $scope.paginator.countPage()) > 0){
                start = start - ((interval/2+$scope.paginator.page) -$scope. paginator.countPage());
            }

            if (start < 0){
                start = 0;
            }

            input = new Array(end - start);
            for (var i = 0; start < end; start++, i++) {
                input[i] = start;
            }

            return input;
        }
    }])
