/**
 * Created by pavel on 20.10.14.
 */

'use strict';

angular.module('GeoObjectsBundle.services.paginator',[])
    .service('paginator', [function(){
        return {
            limit: 20,
            page: 0,
            count: 0,
            countPage: function (){
                return Math.ceil(parseInt(this.count) / parseInt(this.limit));
            },
            firstPage: function (){
                this.page = 0;
            },
            endPage: function (){
                this.page = this.countPage()-1;
            },
            setPage: function (page){
                this.page = page;
            },
            isFirstPage: function(){
                return (this.page == 0);
            },
            isEndPage: function(){
                return (this.page == (this.countPage()-1));
            }
        }
    }])
