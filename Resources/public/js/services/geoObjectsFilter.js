/**
 * Created by pavel on 20.10.14.
 */

'use strict';

angular.module('GeoObjectsBundle.services.GeoObjectsFilter',[])
    .service('GeoObjectsFilter', function ($rootScope){
        $rootScope.filter = {};

        return {
            get: function (){
                return $rootScope.filter;
            },
            set: function (filter){
                $rootScope.filter = filter;
            },
            offsetExist: function (item){
                return ($rootScope.filter[item] != undefined)?true:false;
            },
            offsetGet: function (item){
                if (this.offsetExist(item)){
                    return $rootScope.filter[item];
                }

                return null;
            },
            offsetSet: function (item, value){
                $rootScope.filter[item] = value;
            },
            offsetRemove: function (item){
                if (this.offsetExist(item)){
                    delete($rootScope.filter[item]);
                }
            }
        }
    });
