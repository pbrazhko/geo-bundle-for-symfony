/**
 * Created by pavel on 20.10.14.
 */

'use strict';

angular.module('GeoObjectsBundle.services.GeoObjectsTypesStore',[])
    .service('GeoObjectsTypesStore', function($http){
        return {
            get: function (){
                return $http.get(Routing.generate('geo_get_types'))
            },
            getByName: function (name){
                return $http.get(Routing.generate('geo_get_type_by_name', {name: name}))
            }
        }
    })
