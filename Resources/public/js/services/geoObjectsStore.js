/**
 * Created by pavel on 20.10.14.
 */

'use strict';

angular.module('GeoObjectsBundle.services.GeoObjectsStore',[])
    .service('GeoObjectsStore', function($http){
        return {
            get: function (filter, limit, skip){
                var data = {};

                data = angular.extend({}, data, filter);

                data['limit'] = limit;
                data['skip'] = skip;

                return $http.get(Routing.generate('geo_get_objects', data));
            },
            getByReference: function (id, type){
                return $http.get(Routing.generate('geo_get_object_by_reference_id', {'id': id, 'type': type}));
            },
            getByDistance: function (lat, lon, distance, metric, type){
                var data = {
                    'geometry': [
                        lat,
                        lon
                    ],
                    'distance': distance
                }

                if (metric != undefined){
                    data.metric = metric;
                }

                if (type != undefined){
                    data.type = type;
                }

                return $http.get(Routing.generate('geo_get_objects_by_distance', data))
            },
            getInfo: function (id){
                return $http.get(Routing.generate('geo_get_info', {id: id}));
            },
            remove: function (id){
                return $http.get(Routing.generate('geo_remove_object', {'id': id}));
            },
            save: function (data){
                return $http({
                    'url': data.id==undefined?Routing.generate('geo_add_object'):Routing.generate('geo_update_object'),
                    'data': data,
                    'method': 'POST'
                });
            }
        }
    })