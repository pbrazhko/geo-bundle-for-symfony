/**
 * Created by pavel on 03.10.14.
 */

'use strict';

(
    function($){
        var geoMaps = {
            config: {
                'type': 'static',
                'staticMapURL': 'http://maps.googleapis.com/maps/api/staticmap',
                'enabledDynamicMap': true,
                'loadBySideObjects': false,
                'allowedAddObjects': false,
                'allowedEditObjects': false,
                'markersIcon': '',
                'drawingModes': ['MARKER'],
                'token': null
            },
            staticMapConfig: {
                center: {
                    lat: 0,
                    lng: 0
                },
                zoom: 12,
                maptype: 'roadmap',
                sensor: true
            },
            dynamicMapConfig: {
                center: {
                    lat: 0,
                    lng: 0
                },
                zoom: 14,
                backgroundColor: '#ffffff',
                disableDoubleClickZoom: false,
                draggable: true,
                scrollwheel: true,
                disableDefaultUI: false
            },
            geometriesOptions: {
                fillColor: '#cccccc',
                fillOpacity: 0.5,
                strokeWeight: 5,
                clickable: true,
                draggable: false,
                editable: true
            },
            impl: {
                staticMap: null,
                dynamicMap: null,
                infoWindows: {},
                config: {},
                dynamicMapConfig: {},
                staticMapConfig: {},
                drawingManager: null,
                geometries: [],
                init: function (mapContainer, config, staticMapConfig, dynamicMapConfig) {
                    if ($.fn.geoMapsFactory == undefined) {
                        throw new Error('Geo maps factory is not fined!');
                    }

                    this.config = $.extend({}, geoMaps.config, config);
                    this.staticMapConfig = $.extend({}, geoMaps.staticMapConfig, staticMapConfig);
                    this.dynamicMapConfig = $.extend({}, geoMaps.dynamicMapConfig, dynamicMapConfig);

                    if (this.config.token == null) {
                        throw new Error('Token is not defined!');
                    }

                    $(window).on('map.resize', this.mapsResize);
                    $(window).on('map.refresh', this.mapRefresh);

                    $(window).trigger('map.init.map', [this]);

                    $(window).on('map.refreshGeometries', this.refreshGeometries);

                    this.initMap(mapContainer);
                },
                initMap: function (container) {
                    if(this.config.type == 'static'){
                        this.initStaticMap(container);
                    }
                    else if(this.config.type == 'dynamic'){
                        this.initDynamicMap(container)
                    }
                    else{
                        throw new Error('Not supported map type!');
                    }
                },
                initStaticMap: function(container){
                    this.staticMap = container;

                    this.staticMap.append(
                        $('<img/>')
                            .attr({
                                'src': this.getStaticMapUrl(this.staticMapConfig, this.staticMap.width(), this.staticMap.height()),
                                'width': this.staticMap.width(),
                                'height': this.staticMap.height()
                            })
                    );

                    if (this.config.loadBySideObjects) {
                        this.loadObjects(this.config.token);
                    }

                    if(this.config.enabledDynamicMap){
                        var element = $('<div/>').css({
                            width: this.getWidth() + 'px',
                            height: this.getHeight() + 'px'
                        });

                        this.dynamicMap = new google.maps.Map(element[0], this.dynamicMapConfig);

                        if ((this.config.allowedAddObjects || this.config.allowedEditObjects) && this.drawingManager == null) {
                            this.initDrawingManager();
                        }

                        this.dynamicMap.setOptions({
                            styles: [{
                                featureType: 'poi.business',
                                stylers: [
                                    { visibility: "off" }
                                ]
                            }]});

                        $(window).trigger('map.refreshGeometries', [this]);


                        if (this.config.enabledDynamicMap) {
                            this.staticMap.on('click', {'self': this}, this.openDynamicMap);
                        }
                    }
                },
                initDynamicMap: function(container){
                    this.dynamicMap = new google.maps.Map(container[0], this.dynamicMapConfig);

                    if ((this.config.allowedAddObjects || this.config.allowedEditObjects) && this.drawingManager == null) {
                        this.initDrawingManager();
                    }

                    this.dynamicMap.setOptions({
                        styles: [{
                            featureType: 'poi.business',
                            stylers: [
                                { visibility: "off" }
                            ]
                        }]});

                    $(window).trigger('map.refreshGeometries', [this]);
                },
                initDrawingManager: function () {
                    this.drawingManager = new google.maps.drawing.DrawingManager();

                    var modes = [];
                    if (this.config.drawingModes.length > 0) {
                        for (var i = 0; i < this.config.drawingModes.length; i++) {
                            modes.push(google.maps.drawing.OverlayType[this.config.drawingModes[i].toUpperCase()]);
                        }
                    }

                    var drawingOptions = {
                        drawingControl: true,
                        drawingControlOptions: {
                            position: google.maps.ControlPosition.TOP_CENTER,
                            drawingModes: modes
                        },
                        circleOptions: geoMaps.geometriesOptions,
                        polygonOptions: geoMaps.geometriesOptions,
                        polylineOptions: geoMaps.geometriesOptions,
                        rectangleOptions: geoMaps.geometriesOptions,
                        markerOptions: {icon: this.config.markersIcon}
                    };

                    this.drawingManager.setOptions(drawingOptions);
                    this.drawingManager.setMap(this.dynamicMap);

                    var self = this;
                    google.maps.event.addListener(this.drawingManager, 'overlaycomplete', function (event) {
                        return self.addGeometry(event.overlay, event.type, self);
                    });
                },
                getStaticMapUrl: function (params, width, height) {
                    var query = '';

                    for (var i in this.geometries['marker']) {
                        var marker = this.geometries['marker'][i];
                        query += 'markers=icon:' + marker.icon + '|' + marker.position.lat() + ',' + marker.position.lng() + '&';
                    }

                    var center = marker == undefined ? {lat: 55.7435219, lng: 37.5918784} : marker.getPosition();

                    params['center'] = center;

                    for (var attr in params) {
                        if (typeof(params[attr]) === 'function') continue;

                        if (attr === 'center') {
                            query += 'center=' + params[attr].lat + ',' + params[attr].lng + '&';
                            continue;
                        }

                        query += attr + '=' + params[attr] + '&';
                    }

                    return this.config.staticMapURL + "?" + query + 'size=' + width + 'x' + height;
                },
                addGeometry: function (geometry, type, self) {
                    if (!$.fn.geoMapsFactory.isSupportedType(type)) {
                        return;
                    }

                    if (typeof(geometry) != 'array' && typeof(geometry) != 'object') {
                        throw new Error('Value of geometries ' + type + ' is not supported');
                    }

                    geometry = $.fn.geoMapsFactory.build(type, geometry);

                    if (self.config.allowedEditObjects) {
                        geometry.setDraggable(true);

                        google.maps.event.addListener(geometry, 'dragend', function (event) {
                            $(window).trigger('map.update_geometry', ['marker', geometry]);
                        });
                    }

                    if (self.geometries[type] === undefined) {
                        self.geometries[type] = [];
                    }

                    self.geometries[type].push(geometry);

                    $(window).trigger('map.add_geometry', [type, geometry]);
                    $(window).trigger('map.refreshGeometries', [self]);
                },
                openDynamicMap: function (event) {
                    var self = event.data.self;
                    var map = self.dynamicMap;

                    if (map === undefined) new Error('Map is not found');

                    $.modal({
                        'title': Translator.trans('Map', {}, 'geo'),
                        'showFooter': false,
                        'content': map.getDiv()
                    });

                    var center = map.getCenter();

                    google.maps.event.trigger(map, "resize");
                    map.setCenter(center);
                },
                refreshGeometries: function (event, self) {
                    for (var type in self.geometries) {
                        if (self.geometries[type].length > 0) {
                            for (var i in self.geometries[type]) {
                                var geometry = self.geometries[type][i];

                                if (geometry.getMap() === undefined) {
                                    geometry.setMap(self.dynamicMap);
                                }

                                geometry.addListener('click', function () {
                                    $(window).trigger('map.onGeometryClick', [this, self.dynamicMap, type, self.config.token]);
                                });
                            }
                        }
                    }
                },
                getWidth: function () {
                    var width = Math.min(
                        document.body.scrollWidth, document.documentElement.scrollWidth,
                        document.body.offsetWidth, document.documentElement.offsetWidth,
                        document.body.clientWidth, document.documentElement.clientWidth
                    );

                    return width / 100 * 90;
                },
                getHeight: function () {
                    var height = Math.min(
                        document.body.scrollHeight, document.documentElement.scrollHeight,
                        document.body.offsetHeight, document.documentElement.offsetHeight,
                        document.body.clientHeight, document.documentElement.clientHeight
                    );

                    return height / 100 * 80;
                },
                mapsResize: function () {
                    var map = geoMaps.impl.dynamicMap;
                    var center = map.getCenter();
                    var div = map.getDiv();

                    div.style.width = geoMaps.impl.getWidth() + 'px';
                    div.style.height = geoMaps.impl.getHeight() + 'px';

                    google.maps.event.trigger(map, "resize");
                    map.setCenter(center);
                },
                mapRefresh: function () {
                    var map = geoMaps.impl.dynamicMap;
                    var center = map.getCenter();

                    google.maps.event.trigger(map, "resize");
                    map.setCenter(center);
                }
            }
        };

        $.fn.geoMaps = function (config, staticMapConfig, dynamicMapConfig) {
            geoMaps.impl.init(this, config, staticMapConfig, dynamicMapConfig);
        };
    })(jQuery);