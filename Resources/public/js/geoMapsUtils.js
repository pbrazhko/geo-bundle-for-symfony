/**
 * Created by pavel on 16.09.15.
 */

(function ($) {
        var geoMapsUtils = {
            geometryFactory: function (builderRegistry) {
                if (builderRegistry == undefined
                    || !builderRegistry instanceof geoMapsUtils.geometryBuildersRegistry) {
                    throw new Error('Builder registry is required argument for constructor');
                }

                this.supportedTypes = new Array();

                this.getSupportedTypes = function () {
                    if (!this.supportedTypes.length) {
                        for (var type in builderRegistry.builders) {
                            this.supportedTypes.push(type);
                        }
                    }

                    return this.supportedTypes;
                };

                this.isSupportedType = function (type) {
                    if (type == '' || type == undefined) {
                        return false;
                    }

                    var types = this.getSupportedTypes();

                    return types.indexOf(type.toUpperCase()) >= 0;
                };

                this.build = function (type, geometry) {
                    if (!this.isSupportedType(type.toUpperCase())) {
                        throw new Error('Not supported geometry type!');
                    }

                    return builderRegistry.getBuilder(type.toUpperCase()).build(geometry);
                };
            },
            geometryBuildersRegistry: function () {
                this.builders = {};

                this.addBuilder = function (type, builder) {
                    type = type.toUpperCase();

                    if (!this.builders.hasOwnProperty(type)) {
                        this.builders[type] = builder;
                    }
                };

                this.getBuilder = function (type) {
                    type = type.toUpperCase();

                    if (this.builders.hasOwnProperty(type)) {
                        return this.builders[type];
                    }

                    return null;
                }
            },
            geometryConverter: function () {
                this.convert = function (geometry, type) {
                    if (typeof (geometry) != "object") {
                        return null;
                    }

                    switch (type) {
                        case 'marker':
                            return [geometry.getPosition().lat(), geometry.getPosition().lng()];
                        case 'polygon':
                        case 'polyline':
                            var _geometry = new Array();

                            var path = geometry.getPath();

                            for (var s = 0; s < path.length; s++) {
                                _geometry.push([path.getAt(s).lat(), path.getAt(s).lng()]);
                            }

                            return _geometry;
                        case 'rectangle':
                            var _geometry = new Array();

                            var bounds = geometry.getBounds();

                            _geometry.push([bounds.getNorthEast().lat(), bounds.getNorthEast().lng()]);
                            _geometry.push([bounds.getNorthEast().lat(), bounds.getSouthWest().lng()]);
                            _geometry.push([bounds.getSouthWest().lat(), bounds.getSouthWest().lng()]);
                            _geometry.push([bounds.getSouthWest().lat(), bounds.getNorthEast().lng()]);
                            _geometry.push([bounds.getNorthEast().lat(), bounds.getNorthEast().lng()]);

                            return _geometry;
                        default :
                            throw new Error('Not supported geometry type!');
                    }
                }
            }
        };

    $.fn.geoMapsGeometryConverter = new geoMapsUtils.geometryConverter();
        $.fn.geoMapsBuildersRegistry = new geoMapsUtils.geometryBuildersRegistry();
        $.fn.geoMapsFactory = new geoMapsUtils.geometryFactory($.fn.geoMapsBuildersRegistry);
    })(jQuery);