/**
 * Created by pavel on 16.09.15.
 */

(function ($) {
        var markerBuilder = function () {
            this.required = ['position'];
            this.options = {};
            this.markerOptions = {
                'id': null,
                'geometryType': 'POINT',
                'provider': null,
                'anchorPoint': null,
                'animation': null,
                'clickable': true,
                'cursor': null,
                'draggable': false,
                'icon': undefined,
                'map': undefined,
                'opacity': 1,
                'optimized': true,
                'place': null,
                'position': {},
                'shape': null,
                'title': '',
                'visible': true,
                'zIndex': null
            };

            this.build = function (geometry) {
                if (typeof(geometry) != 'object') {
                    throw new Error('Marker builder: Not supported type!');
                }

                this.checkRequiredOptions(geometry);

                this.prepareOptions(geometry);

                this.options.position = this.transformPosition(this.options.position);

                return new google.maps.Marker(this.options);
            };

            this.transformPosition = function (position) {
                if (!(position instanceof google.maps.LatLng)) {
                    var positionArray = this.parsePositionString(position);

                    return new google.maps.LatLng(positionArray[0], positionArray[1]);
                }

                return position;
            };

            this.parsePositionString = function (position) {
                if (typeof(position) != 'string') {
                    throw new Error('Position is not string!');
                }

                var regexp = /^[0-9\.\s\,\-e]+$/;

                if (!position.match(regexp)) {
                    throw new Error('Position is not valid!');
                }

                return position.split(/\s|\,\s+/);
            };

            this.checkRequiredOptions = function (geometry) {
                for (var requiredOptionIndex in this.required) {
                    var requiredOption = this.required[requiredOptionIndex];

                    if (!geometry.hasOwnProperty(requiredOption)) {
                        throw new Error('Marker builder: Required option \'' + requiredOption + '\' is not defined!');
                    }
                }
            };

            this.prepareOptions = function (geometry) {
                for (var option in geometry) {
                    var optionValue = geometry[option];

                    this.options = this.markerOptions;

                    if (this.options.hasOwnProperty(option)) {
                        this.options[option] = optionValue;
                    }
                }
            };
        };

        $.fn.geoMapsBuildersRegistry.addBuilder('marker', new markerBuilder());
    })(jQuery);