<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 06.10.14
 * Time: 13:24
 */

namespace CMS\GeoBundle\Compiler;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class GeoObjectsInfoCompiler implements CompilerPassInterface{

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @api
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('cms.geo.info.providers')) {
            return;
        }

        $definition = $container->getDefinition('cms.geo.info.providers');

        $services = $container->findTaggedServiceIds('geo.info.provider');

        foreach($services as $id=>$attributes) {
            $definition->addMethodCall(
                'addProvider',
                array(new Reference($id))
            );
        }
    }
}