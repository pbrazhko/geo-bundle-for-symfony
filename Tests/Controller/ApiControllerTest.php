<?php

namespace CMS\GeoBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;

class ApiControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    public $client = null;

    public function testAddPolygon(){
        $this->client = static::createClient();

        $parameters = [
            'geometry' => [
                [
                    [
                        "25.774252",
                        "-80.190262"
                    ],
                    [
                        "18.466465",
                        "-66.118292"
                    ],
                    [
                        "32.321384",
                        "-64.75737"
                    ],
                    [
                        "25.774252",
                        "-80.190262"
                    ]
                ]
            ],
            "geometry_type" => "polygon",
            "type" => 'RELATED_OBJECTS',
            "reference_id" => 1
        ];

        $this->client->request('POST', '/api/v1/geo/objects/add', $parameters);

        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        if ($response->getStatusCode() == 400){
            $this->checkError($content);
           return;
        }

        $this->assertTrue($response->isSuccessful(), 'Response status: ' . $response->getStatusCode() . "Text: " . $response->getContent());
        $this->assertNotEmpty($content, 'Response is null');

    }

    public function testAddPoint(){
        $this->client = static::createClient();

        $parameters = [
            'geometry' =>
                [
                    "25.400",
                    "-70.350"
                ]
            ,
            "geometry_type" => "point",
            "type" => 'RELATED_OBJECTS',
            "reference_id" => 1
        ];

        $this->client->request('POST', '/api/v1/geo/objects/add', $parameters);

        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        if ($response->getStatusCode() == 400){
            $this->checkError($content);
            return;
        }

        $this->assertTrue($response->isSuccessful(), 'Response status: ' . $response->getStatusCode());

        $this->assertNotEmpty($content, 'Response is null');

        return $content;
    }

    /**
     * @depends testAddPoint
     */
    public function testByObject($id){
        $this->client = static::createClient();

        $parameters = [
            'id' => $id
        ];

        $this->client->request('GET', '/api/v1/geo/objects/by/object', $parameters);

        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        if ($response->getStatusCode() == 400){
            $this->checkError($content);
            return;
        }
        
        $this->checkResponse($response);
        $this->checkContentStructure($content, $parameters);
    }

    public function testByPoint()
    {
        $this->client = static::createClient();

        $parameters = [
            'geometry' => [
                '36.513428',
                '-4.887279'
            ]
        ];

        $this->client->request('GET', '/api/v1/geo/objects/by/point', $parameters);

        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        if ($response->getStatusCode() == 400){
            $this->checkError($content);
            return;
        }
        
        $this->checkResponse($response);
        $this->checkContentStructure($content, $parameters);
    }

    public function testByDistanceKM()
    {
        $this->client = static::createClient();

        $parameters = [
            'geometry' => [
                '36.513428',
                '-4.887279'
            ],
            'distance' => 5,
            'metric' => 'km'
        ];

        $this->client->request('GET', '/api/v1/geo/objects/by/distance', $parameters);

        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        if ($response->getStatusCode() == 400){
            $this->checkError($content);
            return;
        }

        $this->checkResponse($response);
        $this->checkContentStructure($content, $parameters);
    }

    public function testByDistanceKMLimit()
    {
        $this->client = static::createClient();

        $parameters = [
            'geometry' => [
                '36.513428',
                '-4.887279'
            ],
            'distance' => 5,
            'metric' => 'km',
            'limit' => 10
        ];

        $this->client->request('GET', '/api/v1/geo/objects/by/distance', $parameters);

        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        if ($response->getStatusCode() == 400){
            $this->checkError($content);
            return;
        }

        $this->checkResponse($response);
        $this->checkContentStructure($content, $parameters);
    }

    public function testByDistanceMiles()
    {
        $this->client = static::createClient();

        $parameters = [
            'geometry' => [
                '36.513428',
                '-4.887279'
            ],
            'distance' => 5,
            'metric' => 'miles'
        ];

        $this->client->request('GET', '/api/v1/geo/objects/by/distance', $parameters);

        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        if ($response->getStatusCode() == 400){
            $this->checkError($content);
            return;
        }

        $this->checkResponse($response);
        $this->checkContentStructure($content, $parameters);
    }

    public function testByDistanceMilesLimit()
    {
        $this->client = static::createClient();

        $parameters = [
            'geometry' => [
                '36.513428',
                '-4.887279'
            ],
            'distance' => 5,
            'metric' => 'miles',
            'limit' => 10
        ];

        $this->client->request('GET', '/api/v1/geo/objects/by/distance', $parameters);

        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        if ($response->getStatusCode() == 400){
            $this->checkError($content);
            return;
        }

        $this->checkResponse($response);
        $this->checkContentStructure($content, $parameters);
    }

    public function testByMinDistanceOfType(){
        $this->client = static::createClient();

        $parameters = [
            'geometry' => [
                '36.513428',
                '-4.887279'
            ],
            'type' => 'HOTELS'
        ];

        $this->client->request('GET', '/api/v1/geo/objects/by/min/distance', $parameters);

        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        if ($response->getStatusCode() == 400){
            $this->checkError($content);
            return;
        }

        $this->checkResponse($response);
        $this->checkContentStructure($content, $parameters);
    }

    public function testByReference(){
        $this->client = static::createClient();

        $parameters = [
            'id' => 1,
            'type' => 'HOTELS'
        ];

        $this->client->request('GET', '/api/v1/geo/objects/by/reference', $parameters);

        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        if ($response->getStatusCode() == 400){
            $this->checkError($content);
            return;
        }

        $this->checkResponse($response);
        $this->checkContentStructure($content, $parameters);
    }

    public function testById(){
        $this->client = static::createClient();

        $parameters = [
            'id' => 1
        ];

        $this->client->request('GET', '/api/v1/geo/objects/by/id', $parameters);

        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        if ($response->getStatusCode() == 400){
            $this->checkError($content);
            return;
        }

        $this->checkResponse($response);
        $this->checkContentStructure($content, $parameters);
    }

    public function testGetTypes(){
        $this->client = static::createClient();

        $this->client->request('GET', '/api/v1/geo/types');

        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        if ($response->getStatusCode() == 400){
            $this->checkError($content);
            return;
        }

        $this->checkResponse($response);

        $this->assertInternalType('array', $content, 'Response is not array|json');
    }
    
    public function checkError($content){
        $this->assertArrayHasKey('message', $content, 'Index \'message\' is not found');
    }
    
    private function checkResponse($response){
        $content = $response->getContent();

        $this->assertTrue($response->isSuccessful(), 'Response status: ' . $response->getStatusCode());
        $this->assertNotEmpty($content, 'Response is null');

        $contentArray = json_decode($content, true);

        $this->assertInternalType('array', $contentArray, 'Response is not array|json');
    }

    private function checkContentStructure(array $content, array $parameters){
        $this->assertArrayHasKey('count', $content, 'Index \'count\' is not found');
        $this->assertArrayHasKey('data', $content, 'Index \'data\'  is not found');

        $this->assertInternalType('array', $content['data'], 'Index \'data\' is not array');

        if (!isset($parameters['limit'])){
            $this->assertEquals($content['count'], count($content['data']), 'Index \'count\' is not equals count index \'data\'');
        }
        else{
            $this->assertTrue($parameters['limit'] >= count($content['data']), 'Count \'data\' > limit');
        }
    }

    public function printMethod(){
       $this->writeConsole("Run test: ". $this->getName(), "\033[42m");
    }

    public function printResponse(){

        $marker = "\033[32m";
        $response   = $this->client->getResponse();
        $parameters = $this->client->getRequest()->request->all();

        $this->writeConsole("Request method: ". $this->client->getRequest()->getMethod());
        $this->writeConsole("URL: ". $this->client->getRequest()->getPathInfo() . "?" . $this->client->getRequest()->getQueryString());

        if (count($parameters) > 0){
            $this->writeConsole("Send: ");
            foreach($parameters as $name => $value){
                if (is_array($value)) $value = $value = print_r($value, true);

                $this->writeConsole($name . ": " . $value);
            }
        }

        if ($response->getStatusCode() != 200){
            $marker = "\033[31m";
        }

        $this->writeConsole("Response status: " . $this->client->getResponse()->getStatusCode(), $marker );
        $this->writeConsole("Response content: " . $this->client->getResponse()->getContent(), $marker);
        $this->writeConsole("\n\n");
    }

    private function writeConsole($text, $marker = null){
        fwrite(STDOUT, $marker . $text . "\033[0m\n" );
    }
}
