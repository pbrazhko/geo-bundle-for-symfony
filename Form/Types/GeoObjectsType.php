<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 20.11.15
 * Time: 11:05
 */

namespace CMS\GeoBundle\Form\Types;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GeoObjectsType extends AbstractType
{
    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'type' => 'cms_geo_geoobject_type',
            'allow_add' => true,
            'allow_delete' => true,
            'delete_empty' => true
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'collection';
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'cms_geo_geoobjects_type';
    }
}