<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 20.11.15
 * Time: 11:41
 */

namespace CMS\GeoBundle\Form\Types;


use CMS\GeoBundle\Services\GeoObjectsService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GeometryType extends AbstractType
{
    /**
     * @var GeoObjectsService
     */
    private $geoObjectsService;

    /**
     * GeometryType constructor.
     * @param GeoObjectsService $geoObjectsService
     */
    public function __construct(GeoObjectsService $geoObjectsService)
    {
        $this->geoObjectsService = $geoObjectsService;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('geometry_type', HiddenType::class)
            ->add('geom', HiddenType::class);

        $builder->get('geom')->addModelTransformer(new CallbackTransformer(
            function ($geom) {
                return (string)$geom;
            },
            function ($geom) {
                return $geom;
            }
        ));
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\GeoBundle\Entity\Geometry'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getBlockPrefix()
    {
        return 'cms_geo_geometry_type';
    }
}