<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 08.09.14
 * Time: 12:46
 */

namespace CMS\GeoBundle\Services;


class DistanceConverter {
    const latitude = 69;
    const mile = 1.6;

    /**
     * Convert km to miles
     *
     * @param $km
     * @return float
     */
    static public function convertToMiles($km){
        return $km/DistanceConverter::mile;
    }

    /**
     * Convert miles to km
     *
     * @param $miles
     * @return mixed
     */
    static public function convertToKM($miles){
        return $miles*DistanceConverter::mile;
    }

    /**
     * Convert km to latitude
     *
     * @param $km
     * @return float
     */
    static public function convertToLatitudeFromKM($km){
        return self::convertToMiles($km)/DistanceConverter::latitude;
    }

    /**
     * Convert miles to latitude
     *
     * @param $miles
     * @return float
     */
    static public function convertToLatitudeFromMiles($miles){
        return $miles/DistanceConverter::mile;
    }

} 