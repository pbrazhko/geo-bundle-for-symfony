<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 07.10.14
 * Time: 11:37
 */

namespace CMS\GeoBundle\Services;

use CMS\GeoBundle\Cache\RedisCache;
use CMS\GeoBundle\Normalizer\GeoObjectsInfoNormalizer;
use CMS\GeoBundle\Normalizer\ObjectsNormalizer;
use CMS\GeoBundle\Normalizer\TypesNormalizer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

abstract class AbstractService
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var
     */
    private $serializer;

    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Get geo objects repository
     *
     * @return \CMS\GeoBundle\Entity\Repository\GeoobjectsRepository
     */
    protected function getObjectsRepository($repositoryName)
    {
        return $this->getEntityManager()->getRepository($repositoryName);
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager()
    {
        if (null === $this->em) {
            $this->em = $this->container->get('doctrine.orm.entity_manager');
        }

        return $this->em;
    }
} 