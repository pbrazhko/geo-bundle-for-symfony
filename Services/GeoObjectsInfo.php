<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 06.10.14
 * Time: 13:33
 */

namespace CMS\GeoBundle\Services;

use CMS\GeoBundle\GeoObjectsInfoInterface;

class GeoObjectsInfo {

    private $providers;

    public function __construct(){
        $this->providers = [];
    }

    public function addProvider($provider){
        if ($provider instanceof GeoObjectsInfoInterface){
            $this->providers[strtoupper($provider->getTypeName())] = $provider;
        }

        return $this;
    }

    public function getProvider($alias){
        if (isset($this->providers[strtoupper($alias)])) {
            return $this->providers[strtoupper($alias)];
        }

        return null;
    }

    public function isExists($alias){
        return isset($this->providers[strtoupper($alias)]);
    }

    public function getAll(){
        return $this->providers;
    }
} 