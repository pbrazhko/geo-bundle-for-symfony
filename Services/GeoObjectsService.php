<?php
namespace CMS\GeoBundle\Services;

use CMS\CoreBundle\AbstractCoreService;
use CMS\GeoBundle\Entity\Geometry;
use CMS\GeoBundle\Entity\Geotypes;
use CMS\GeoBundle\Exceptions\InfoProviderNotFoundException;
use CMS\GeoBundle\Exceptions\InvalidArgumentException;
use CMS\GeoBundle\Exceptions\ObjectNotFoundException;
use CMS\GeoBundle\GeoObjectsInfoInterface;
use CMS\GeoBundle\Response\GeoObjectsInfoResponse;
use CMS\GeoBundle\Response\GeoObjectsListResponse;
use CMS\GeoBundle\Types\GeometryInterface;
use CMS\GeoBundle\Types\MultiPolygon;
use CMS\GeoBundle\Types\Point;
use CMS\GeoBundle\Types\Polygon;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;

class GeoObjectsService extends AbstractCoreService
{
    /**
     * Get objects by point
     *
     * @param Point $point
     * @param string|null $type
     * @param null $geometry_type
     * @return array
     */
    public function getByPoint(Point $point, $type = null, $geometry_type = null)
    {
        $repo = $this->getObjectsRepository('GeoBundle:Geoobjects');

        $type = $this->getGeoType($type);

        return $repo->getByPoint($point, $type, $geometry_type);
    }

    /**
     * Get objects by object polygon
     *
     * @param \CMS\GeoBundle\Entity\Geometry $object
     * @param string|null $type
     * @param bool $asText Variable polygon text or binary
     * @param int|null $skip
     * @param int|null $limit
     * @param null $geometry_type
     * @return array
     */
    public function getByGeometryObject(Geometry $object, $type = null, $asText = true, $skip = null, $limit = null, $geometry_type = null)
    {
        $type = $this->getGeoType($type);

        $repo = $this->getObjectsRepository('GeoBundle:Geoobjects');

        if ($asText) {
            $result = $repo->getByGeometryObjectAsText($object, $type, $skip, $limit, $geometry_type);
        } else {
            $result = $repo->getByGeometryObjectAsBinary($object, $type, $skip, $limit, $geometry_type);
        }

        return $result;
    }

    /**
     * Get all geo types
     *
     * @param string $name
     * @return GeoObjectsInfoInterface
     */
    public function getGeoType($name)
    {
        $geoProvidersService = $this->container->get('cms.geo.info.providers');

        if ($geoProvidersService->isExists($name)){
            return $geoProvidersService->getProvider($name);
        }

        return null;
    }

    /**
     * Return geometry for object
     *
     * @param int $id
     * @param bool $asText
     * @return mixed
     * @throws \CMS\GeoBundle\Exceptions\InvalidArgumentException
     */
    public function getGeometryObject($id, $asText = true)
    {
        if (!is_numeric($id)) {
            throw new InvalidArgumentException('Type variable \'id\' is not supported!');
        }

        $repo = $this->getObjectsRepository('GeoBundle:Geoobjects');

        if ($asText) {
            $result = $repo->getGeometryObjectAsText($id);
        } else {
            $result = $repo->getGeometryObjectAsBinary($id);
        }

        return $result;
    }

    /**
     * Get objects by distance of km.
     *
     * @param Point $point
     * @param int $distance Distance in km
     * @param string|null $type
     * @param int $skip
     * @param int $limit
     * @param $geometry_type
     * @throws InvalidArgumentException
     * @return array|\CMS\GeoBundle\Result\ORMResultNative
     */
    public function getByDistanceKM(Point $point, $distance, $type = null, $skip = null, $limit = null, $geometry_type = null)
    {
        if (!is_numeric($distance)) {
            throw new InvalidArgumentException(sprintf('Variable type "distance" is not supported!'));
        }

        $type = $this->getGeoType($type);

        $distance = DistanceConverter::convertToLatitudeFromKM($distance);

        return $this->getByDistance($point, $distance, $type, $skip, $limit, $geometry_type);
    }

    /**
     * Get geo objects by distance
     *
     * @param Point $point
     * @param int $distance Distance
     * @param GeoObjectsInfoInterface $type Type of geo object
     * @param int $skip
     * @param int $limit
     * @param null $geometry_type
     * @return array|\CMS\GeoBundle\Result\ORMResultNative
     */
    private function getByDistance(Point $point, $distance, GeoObjectsInfoInterface $type = null, $skip = null, $limit = null, $geometry_type = null)
    {
        $repo = $this->getObjectsRepository('GeoBundle:Geoobjects');

        $result = $repo->getCircleAsText($point, $distance);

        if (!$result) {
            return array();
        }

        $result = $repo->getByGeometryObjectAsText($result->getSingleResult(), $type, $skip, $limit, $geometry_type);

        return $result;
    }

    /**
     * Get geo objects by distance of miles.
     *
     * @param Point $point
     * @param $distance Distance in miles
     * @param string|null $type
     * @param $skip
     * @param $limit
     * @param null $geometry_type
     * @throws InvalidArgumentException
     * @return array|\CMS\GeoBundle\Result\ORMResultNative
     */
    public function getByDistanceMiles(Point $point, $distance, $type = null, $skip = null, $limit = null, $geometry_type = null)
    {
        if (!is_numeric($distance)) {
            throw new InvalidArgumentException(sprintf('Variable type "distance" is not supported!'));
        }

        $type = $this->getGeoType($type);

        $distance = DistanceConverter::convertToLatitudeFromMiles($distance);

        return $this->getByDistance($point, $distance, $type, $skip, $limit, $geometry_type);
    }

    /**
     * Get geo objects by min distance of point and type
     *
     * @param Point $point
     * @param string $type
     * @return \CMS\GeoBundle\Result\ORMResultNative
     * @throws \CMS\GeoBundle\Exceptions\InvalidArgumentException
     */
    public function getByMinDistanceOfType(Point $point, $type)
    {
        if (null === ($type = $this->getGeoType($type))) {
            throw new InvalidArgumentException(sprintf('Type is not exists!'));
        }

        $repo = $this->getObjectsRepository('GeoBundle:Geoobjects');

        return $repo->getByMinDistanceOfType($point, $type);
    }

    /**
     * Add geo object
     *
     * @param \CMS\GeoBundle\Entity\Geometry|\CMS\GeoBundle\Types\GeometryInterface $geometry
     * @param string $type
     * @param string $reference_id
     * @throws \CMS\GeoBundle\Exceptions\InvalidArgumentException
     * @return bool
     */
    public function addGeoobject(GeometryInterface $geometry, $type, $reference_id)
    {
        if (null === $reference_id) {
            throw new InvalidArgumentException('Argument "reference_id" is required!');
        }

        if (null === ($type = $this->getGeoType($type))) {
            throw new InvalidArgumentException(sprintf('Type is not exists!'));
        }

        $repo = $this->getObjectsRepository('GeoBundle:Geoobjects');

        return $repo->addGeoobject($geometry, $type, $reference_id);
    }

    /**
     * Update geo object
     *
     * @param int $id
     * @param GeometryInterface $geometry
     * @param string|null $type
     * @param string|null $reference_id
     * @return \Doctrine\DBAL\Driver\Statement
     * @throws \CMS\GeoBundle\Exceptions\InvalidArgumentException
     */
    public function updateGeoobject($id, GeometryInterface $geometry, $type = null, $reference_id = null)
    {
        $type = $this->getGeoType($type);

        $repo = $this->getObjectsRepository('GeoBundle:Geoobjects');

        return $repo->updateGeoobject($id, $geometry, $type, $reference_id);
    }

    /**
     * Create geo object
     *
     * @param Request $request
     * @param string $type Geometry type
     * @throws InvalidArgumentException
     * @return bool|null|Point|Polygon
     */
    public function createGeometry(Request $request, $type)
    {
        $object = null;

        switch (strtolower($type)) {
            case 'polygon':
                $object = new Polygon($request);
                break;
            case 'multipolygon':
                $object = new MultiPolygon($request);
                break;
            case 'point':
            case 'marker':
                $object = new Point($request);
                break;
            default:
                throw new InvalidArgumentException(sprintf('Not supported geometry type \'%s\'', $type));
        }

        return $object;
    }

    /**
     * Get geo object by reference id
     *
     * @param array|int $id
     * @param string|int $type
     * @return \CMS\GeoBundle\Result\ORMResultNative
     * @throws \CMS\GeoBundle\Exceptions\InvalidArgumentException
     * @throws \CMS\GeoBundle\Exceptions\ObjectNotFoundException
     */
    public function getByReference($id, $type)
    {
        if (null === ($type = $this->getGeoType($type))) {
            throw new InvalidArgumentException(sprintf('Type is not exists!'));
        }

        $repo = $this->getObjectsRepository('GeoBundle:Geoobjects');

        $result = $repo->getByReference($id, $type);

        if (!$result) {
            throw new ObjectNotFoundException(sprintf('Object (reference_id: %s) not found!', $id));
        }

        return $result;
    }

    /**
     * Remove geo object
     *
     * @param int $id
     * @throws InvalidArgumentException
     * @throws ObjectNotFoundException
     * @return bool
     */
    public function removeGeoobject($id)
    {
        if (!is_numeric($id)) {
            throw new InvalidArgumentException('Variable type "id" is not supported!');
        }

        $repo = $this->getObjectsRepository('GeoBundle:Geoobjects');

        $result = $this->getById($id);

        if (!$result) {
            throw new ObjectNotFoundException(sprintf('Object (id: %s) not found!', $id));
        }

        return $repo->removeGeoobject($result->getSingleResult());
    }

    /**
     * Get geo object by id
     *
     * @param int $id
     * @throws InvalidArgumentException
     * @throws ObjectNotFoundException
     * @return \CMS\GeoBundle\Result\ORMResultNative
     */
    public function getById($id)
    {
        if (null === $id || empty($id)) {
            throw new InvalidArgumentException('Variable "id" is required!');
        }

        $repo = $this->getObjectsRepository('GeoBundle:Geoobjects');

        $result = $repo->getById($id);

        if (count($result) <= 0) {
            throw new ObjectNotFoundException(sprintf('Object (id: %s) not found!', $id));
        }

        return $result;
    }

    /**
     * @param string $geoTypeName
     * @param string|null $query
     * @param int|null $limit
     * @param int|null $skip
     * @return array|GeoObjectsListResponse
     * @throws InfoProviderNotFoundException
     */
    public function getReferenceObjectsList($geoTypeName, $query = null, $limit = null, $skip = null){
        $infoProviders = $this->container->get('geo.info.providers');

        if (null === ($provider = $infoProviders->getProvider($geoTypeName))) {
            throw new InfoProviderNotFoundException(
                sprintf('Info provider for \'%s\' not found!', $geoTypeName)
            );
        }

        try {
            $list = $provider->getObjectsList($query, $limit, $skip);

            if (null === $list) {
                return [];
            }
        } catch (\Exception $e) {
            throw new \RuntimeException($e->getMessage());
        }

        if (!$list instanceof GeoObjectsListResponse){
            throw new \RuntimeException(sprintf('Response objects list provider is not supported!'));
        }

        return $list;
    }

    /**
     * @param int $id
     * @param $provider
     * @return array|\CMS\GeoBundle\Result\ORMResultNative|string
     * @throws InfoProviderNotFoundException
     */
    public function getInfo($id, $provider)
    {
        $infoProviders = $this->container->get('cms.geo.info.providers');

        $geoObject = $this->getRepository()->findOneBy(['id' => $id]);

        if (null === ($provider = $infoProviders->getProvider($provider))) {
            throw new InfoProviderNotFoundException(
                sprintf('Info provider for \'%s\' not found!', $geoObject->getGeotype())
            );
        }

        try {
            $info = $provider->getGeoObjectsInfo($geoObject);
        } catch (\Exception $e) {
            throw new \RuntimeException($e->getMessage());
        }

        return $info;
    }

    /**
     * Return all geo objects
     *
     * @param string|null $type
     * @param int|null $limit
     * @param int|null $skip
     * @param null $geometry_type
     * @throws ObjectNotFoundException
     * @return \CMS\GeoBundle\Result\ORMResultNative
     */
    public function getAll($type = null, $limit = null, $skip = null, $geometry_type = null){
        $repo = $this->getObjectsRepository('GeoBundle:Geoobjects');

        $type = $this->getGeoType($type);

        $result = $repo->getAll($type, $limit, $skip, $geometry_type);

        if (count($result) <= 0) {
            throw new ObjectNotFoundException(sprintf('Objects not found!'));
        }

        return $result;
    }

    /**
     * Create geometry from string
     *
     * @param string $geometry
     * @return null|Point|Polygon|Multipolygon
     * @throws \CMS\GeoBundle\Exceptions\InvalidArgumentException
     */
    public function createGeometryFromString($geometry)
    {
        if (!preg_match('/^(MULTIPOLYGON|POLYGON|POINT)\(/', $geometry, $matches)) {
            return null;
        }

        $object = null;

        switch ($matches[1]) {
            case 'POLYGON':
                $object = new Polygon();
                $object->fromString($geometry);
                break;
            case 'MULTIPOLYGON':
                $object = new MultiPolygon();
                $object->fromString($geometry);
                break;
            case 'POINT':
                $object = new Point();
                $object->fromString($geometry);
                break;
            default:
                throw new InvalidArgumentException('Geometry is not supported!');
        }

        return $object;
    }

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return [];
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form;
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'GeoBundle:Geoobjects';
    }
}