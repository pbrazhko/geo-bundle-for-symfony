<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 10.09.15
 * Time: 15:51
 */

namespace CMS\GeoBundle\Services;


use Symfony\Component\HttpFoundation\Session\SessionInterface;

class GeoMapsService
{
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function addMap()
    {
        $token = $this->createToken();

        $maps = $this->session->get('maps', array());

        if (!in_array($token, $maps)) {
            $maps[] = $token;
        }

        $this->session->set('maps', $maps);

        return $token;
    }

    public function isExists($token)
    {
        $maps = $this->session->get('maps', array());

        return in_array($token, $maps);
    }

    public function createToken()
    {
        $maps = $this->session->get('maps');

        return md5('pegas_geo_map_' . count($maps));
    }
}