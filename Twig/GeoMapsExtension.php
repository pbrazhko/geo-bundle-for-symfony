<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 30.09.14
 * Time: 18:46
 */

namespace CMS\GeoBundle\Twig;


use CMS\GeoBundle\Entity\Geoobjects;
use CMS\GeoBundle\Exceptions\InvalidArgumentException;
use CMS\GeoBundle\Exceptions\ObjectNotFoundException;
use CMS\GeoBundle\Types\AbstractGeometry;
use CMS\GeoBundle\Types\Point;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GeoMapsExtension extends \Twig_Extension{

    private $container;

    private $staticMapParameters = array(
        'zoom' => 14,
        'width' => 500,
        'height' => 450,
        'loadBesideObjects' => false,
        'besideObjectsType' => null,
        'distance' => 5
    );

    public function __construct(ContainerInterface $container){
        $this->container = $container;
    }

    public function getFunctions(){
        return [
            new \Twig_SimpleFunction('renderStaticMap', [$this, 'renderStaticMap'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new \Twig_SimpleFunction('renderDynamicMap', [$this, 'renderDynamicMap'], ['is_safe' => ['html'], 'needs_environment' => true])
        ];
    }

    public function renderStaticMap(\Twig_Environment $environment, $reference, $type, array $parameters = array()){
        $resolver = new OptionsResolver();
        $resolver->setDefaults($this->staticMapParameters);

        $parameters = $resolver->resolve($parameters);

        $geoService = $this->container->get('geo.objects');

        $geoObjects = [];

        try {
            $result = $geoService->getByReference($reference, $type);
        }
        catch(InvalidArgumentException $e){
            return null;
        }
        catch(ObjectNotFoundException $e){
            return null;
        }

        if (null === ($geoObject = $result->getSingleResult())){
            return null;
        }

        $center = $geoObject->getGeometry();

        if ($parameters['loadBesideObjects']){
            try {
                $result = $geoService->getByDistanceKM($center->getGeom(), $parameters['distance'], $parameters['besideObjectsType']);

                $geoObjects = array_merge($geoObjects, $result->getResult());
            }
            catch(InvalidArgumentException $e){
                $besideObjects = null;
            }
        }

        return $environment->render('GeoBundle:Map:static_map.html.twig', [
            'url' => $this->generateUrlStaticMap($center->getGeom(), $geoObjects, $parameters)
        ]);
    }

    public function renderDynamicMap(\Twig_Environment $environment, $reference, $type, array $parameters = array()){
        $resolver = new OptionsResolver();
        $resolver->setDefaults($this->staticMapParameters);

        $parameters = $resolver->resolve($parameters);

        return $environment->render('GeoBundle:Map:dynamic_map.html.twig', [
                'reference' => $reference,
                'type' => $type,
                'parameters' => $parameters
            ]);
    }


    private function generateUrlStaticMap(AbstractGeometry $center, array $markers = array(), array $parameters)
    {
        $url = 'http://maps.googleapis.com/maps/api/staticmap?';

        if (!$center instanceof Point){
            return null;
        }

        $centerCoords = $center->toArray();
        $url .= 'center=' . $centerCoords[0] . ', ' . $centerCoords[1] . '&';

        if (count($markers) > 0){
            foreach($markers as $marker){
                if (!$marker instanceof Geoobjects) continue;

                if (null === ($geometry = $marker->getGeometry())){
                    continue;
                }

                if (!($geometry = $geometry->getGeom()) instanceof Point){
                    continue;
                }

                $coords = $geometry->toArray();
                $url .= 'markers=|' . $coords[0] . ',' . $coords[1] . '&';
            }
        }
        else{
            $url .= '&zoom=' . $parameters['zoom'];
        }

        $url .= '&size=' . $parameters['width'] . 'x' . $parameters['height'];

        return $url;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'geo_maps_extension';
    }
}