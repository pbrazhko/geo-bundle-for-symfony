<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 24.11.15
 * Time: 20:08
 */

namespace CMS\GeoBundle\Listener;


use CMS\GeoBundle\Entity\Geometry;
use CMS\GeoBundle\Services\GeoObjectsService;
use CMS\GeoBundle\Types\GeometryInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;

class GeometryListener
{
    /**
     * @var GeoObjectsService
     */
    private $geoObjectsService;

    /**
     * GeometryListener constructor.
     * @param $geoObjectsService
     */
    public function __construct(GeoObjectsService $geoObjectsService)
    {
        $this->geoObjectsService = $geoObjectsService;
    }

    public function prePersist(LifecycleEventArgs $event){
        $entity = $event->getEntity();

        if($entity instanceof Geometry){
            $geom = $entity->getGeom();

            if (!$geom instanceof GeometryInterface) {
                $entity->setGeom($this->geoObjectsService->createGeometryFromString(
                    sprintf('%s(%s)', strtoupper($entity->getGeometryType()), $geom)
                ));
            }
        }

        return true;
    }

    public function preUpdate(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();

        if ($entity instanceof Geometry) {
            $geom = $entity->getGeom();

            if (!$geom instanceof GeometryInterface) {
                $entity->setGeom($this->geoObjectsService->createGeometryFromString(
                    sprintf('%s(%s)', strtoupper($entity->getGeometryType()), $geom)
                ));
            }
        }

        return true;
    }
}