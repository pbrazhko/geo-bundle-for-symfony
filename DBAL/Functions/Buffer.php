<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 04.09.14
 * Time: 17:05
 */

namespace CMS\GeoBundle\DBAL\Functions;


use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

class Buffer extends FunctionNode {

    private $point;
    private $distance;
    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     *
     * @return string
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return 'Buffer(' . $sqlWalker->walkStringPrimary($this->point) . ', '  . $sqlWalker->walkStringPrimary($this->distance) . ')';
    }

    /**
     * @param \Doctrine\ORM\Query\Parser $parser
     *
     * @return void
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER); // Buffer
        $parser->match(Lexer::T_OPEN_PARENTHESIS); // (
        $this->point = $parser->StringPrimary(); // point
        $parser->match(Lexer::T_COMMA); // ,
        $this->distance = $parser->StringPrimary(); // distance
        $parser->match(Lexer::T_CLOSE_PARENTHESIS); // )
    }
}