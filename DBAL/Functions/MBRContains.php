<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 04.09.14
 * Time: 15:10
 */

namespace CMS\GeoBundle\DBAL\Functions;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

class MBRContains extends FunctionNode{

    private $g1;
    private $g2;
    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     *
     * @return string
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return 'MBRContains(' . $sqlWalker->walkArithmeticPrimary($this->g1) . ', ' . $sqlWalker->walkArithmeticPrimary($this->g2) . ')';
    }

    /**
     * @param \Doctrine\ORM\Query\Parser $parser
     *
     * @return void
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER); //MBRContains
        $parser->match(Lexer::T_OPEN_PARENTHESIS); // (
        $this->g1 = $parser->StringPrimary(); // geometry
        $parser->match(Lexer::T_COMMA); // ,
        $this->g2 = $parser->StringPrimary(); // geometry
        $parser->match(Lexer::T_CLOSE_PARENTHESIS); // )
    }
}