<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 04.09.14
 * Time: 17:05
 */

namespace CMS\GeoBundle\DBAL\Functions;


use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

class AsBinary extends FunctionNode {

    private $fieldName;
    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     *
     * @return string
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return 'AsBinary(' . $sqlWalker->walkStringPrimary($this->fieldName) . ')';
    }

    /**
     * @param \Doctrine\ORM\Query\Parser $parser
     *
     * @return void
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER); // asBinary
        $parser->match(Lexer::T_OPEN_PARENTHESIS); // (
        $this->fieldName = $parser->StringPrimary(); // field name
        $parser->match(Lexer::T_CLOSE_PARENTHESIS); // )
    }
}