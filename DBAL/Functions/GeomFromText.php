<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 04.09.14
 * Time: 16:01
 */

namespace CMS\GeoBundle\DBAL\Functions;


use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

class GeomFromText extends FunctionNode{

    private $g;
    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     *
     * @return string
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return 'GeomFromText(' . $sqlWalker->walkStringPrimary($this->g) . ')';
    }

    /**
     * @param \Doctrine\ORM\Query\Parser $parser
     *
     * @return void
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER); //GeomFromText
        $parser->match(Lexer::T_OPEN_PARENTHESIS); // (
        $this->g = $parser->StringPrimary(); // geometry
        $parser->match(Lexer::T_CLOSE_PARENTHESIS); // )
    }
}