<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 04.09.14
 * Time: 17:05
 */

namespace CMS\GeoBundle\DBAL\Functions;


use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

class Point extends FunctionNode {

    private $lon;
    private $lat;
    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     *
     * @return string
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return 'POINT(' . $sqlWalker->walkStringPrimary($this->lon) . ', ' . $sqlWalker->walkStringPrimary($this->lat) . ')';
    }

    /**
     * @param \Doctrine\ORM\Query\Parser $parser
     *
     * @return void
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER); // POINT
        $parser->match(Lexer::T_OPEN_PARENTHESIS); // (
        $this->lon = $parser->StringPrimary(); // lon
        $parser->match(Lexer::T_COMMA);
        $this->lat = $parser->StringPrimary(); // lat
        $parser->match(Lexer::T_CLOSE_PARENTHESIS); // )
    }
}