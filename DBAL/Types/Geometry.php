<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 04.09.14
 * Time: 12:23
 */

namespace CMS\GeoBundle\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use CMS\GeoBundle\Types\MultiPolygon;
use CMS\GeoBundle\Types\Point;
use CMS\GeoBundle\Types\Polygon;

class Geometry extends Type{

    const GEOMETRY = 'geometry';

    /**
     * Gets the SQL declaration snippet for a field of this type.
     *
     * @param array $fieldDeclaration The field declaration.
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform The currently used database platform.
     *
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'geometry';
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return mixed|string
     */
    public function convertToPHPValueSQL($value, $platform){
        return sprintf('asText(%s)', $value);
    }

    /**
     * @param string $sqlExpr
     * @param AbstractPlatform $platform
     * @internal param mixed $value
     * @return mixed|string
     */
    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform){
        return sprintf('GeomFromText(%s)', $sqlExpr);
    }

    /**
     * Converts a value from its PHP representation to its database representation
     * of this type.
     *
     * @param mixed $value The value to convert.
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform The currently used database platform.
     *
     * @return mixed The database representation of the value.
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return null === $value ?: $value->getGeometryType() . ' (' . (string)$value . ')';
    }

    /**
     * Does working with this column require SQL conversion functions?
     *
     * This is a metadata function that is required for example in the ORM.
     * Usage of {@link convertToDatabaseValueSQL} and
     * {@link convertToPHPValueSQL} works for any type and mostly
     * does nothing. This method can additionally be used for optimization purposes.
     *
     * @return boolean
     */
    public function canRequireSQLConversion()
    {
        return true;
    }


    public function convertToPHPValue($value, AbstractPlatform $platform){
        if (!preg_match('/^(POLYGON|MULTIPOLYGON|POINT)\(/', $value, $matches)){
            return $value;
        }

        $geometry = null;

        switch($matches[1]){
            case 'POLYGON':
                $geometry = new Polygon();
                break;
            case 'MULTIPOLYGON':
                $geometry = new MultiPolygon();
                break;
            case 'POINT';
                $geometry = new Point();
                break;
        }

        if (null === $geometry){
            return $value;
        }

        $geometry->fromString($value);

        return $geometry;
    }

    /**
     * Gets the name of this type.
     *
     * @return string
     *
     * @todo Needed?
     */
    public function getName()
    {
        return self::GEOMETRY;
    }
}