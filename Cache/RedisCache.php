<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 07.10.14
 * Time: 12:12
 */

namespace CMS\GeoBundle\Cache;

use CMS\GeoBundle\Exceptions\InvalidArgumentException;
use Predis\Client;

class RedisCache {

    private $redisClient;

    private $namespace;

    public function __construct(Client $client, $namespace){
        $this->redisClient = $client;

        if (!is_string($namespace) || empty($namespace)){
            throw new InvalidArgumentException('Namespace for redis cache is not valid!');
        }

        $this->namespace = str_replace('::',':', $namespace);
    }

    public function get($key){
        if (!is_string($key)){
            return false;
        }

        try {
            if (!$this->redisClient->exists($this->namespace . ':' .  $key)){
                return null;
            }

            $result = $this->redisClient->get($this->namespace . ':' . $key);
        }
        catch(\Exception $e){
            return null;
        }

        return $result;
    }

    public function set($key, $value){

        if (!is_string($key)){
            return false;
        }

        try{
            $result = $this->redisClient->set($this->namespace . ':' .  $key, $value);
        }
        catch(\Exception $e){
           return false;
        }

        return $result;
    }

    public function generateKey($args){
        if (is_array($args)){
            $args = implode('', array_values($args));
        }

        return md5($args);
    }
} 