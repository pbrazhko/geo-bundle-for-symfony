<?php

namespace CMS\GeoBundle\Controller;

use CMS\GeoBundle\Exceptions\InfoProviderNotFoundException;
use CMS\GeoBundle\Exceptions\InvalidArgumentException;
use CMS\GeoBundle\Exceptions\ObjectNotFoundException;
use CMS\GeoBundle\Response\ErrorJsonResponse;
use CMS\GeoBundle\Response\GeoJsonResponse;
use CMS\GeoBundle\Types\Point;
use Doctrine\DBAL\DBALException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends Controller
{
    /**
     * Get objects by point
     *
     * @param Request $request
     * @return Response
     */
    public function byPointAction(Request $request)
    {
        $geoObjectsService = $this->get('geo.objects');

        $result = $geoObjectsService->getByPoint(
            new Point($request),
            $request->get('type'),
            $request->get('geometry_type')
        );

        return new GeoJsonResponse($result);
    }

    /**
     * Get all objects by object
     *
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function byObjectAction(Request $request)
    {
        $geoObjectsService = $this->get('geo.objects');

        try {
            $result = $geoObjectsService->getGeometryObject($request->get('id'), true);

        } catch (InvalidArgumentException $e) {
            return new ErrorJsonResponse($e->getMessage());
        }

        if (!$result) {
            return new ErrorJsonResponse(sprintf('Object of ID "%s" not found!', $request->get('id')));
        }

        $result = $geoObjectsService->getByGeometryObject(
            $result->getSingleResult(),
            $request->get('type'),
            true,
            $request->get('skip'),
            $request->get('limit'),
            $request->get('geometry_type')
        );

        return new GeoJsonResponse($result);
    }

    /**
     * Get objects by distance
     *
     * @param Request $request
     * @param string  $metric
     * @return Response
     */
    public function byDistanceAction(Request $request, $metric = 'km')
    {
        $geoObjectsService = $this->get('geo.objects');

        if ($metric == 'km') {
            $result = $geoObjectsService->getByDistanceKM(
                new Point($request),
                $request->get('distance'),
                $request->get('type'),
                $request->get('skip'),
                $request->get('limit'),
                $request->get('geometry_type')
            );

        } else {
            if ($metric == 'miles') {
                $result = $geoObjectsService->getByDistanceMiles(
                    new Point($request),
                    $request->get('distance'),
                    $request->get('type'),
                    $request->get('skip'),
                    $request->get('limit'),
                    $request->get('geometry_type')
                );

            } else {
                return new ErrorJsonResponse(sprintf('Type "%s" of metric is not supported!', $metric));
            }
        }

        return new GeoJsonResponse($result);
    }

    /**
     * Get objects by min distance of point and type
     *
     * @param Request $request
     * @return ErrorJsonResponse|GeoJsonResponse
     */
    public function byMinDistanceOfTypeAction(Request $request)
    {
        $geoObjectsService = $this->get('geo.objects');

        try {
            $result = $geoObjectsService->getByMinDistanceOfType(new Point($request), $request->get('type'));

        } catch (InvalidArgumentException $e) {
            return new ErrorJsonResponse($e->getMessage());
        }

        return new GeoJsonResponse($result);
    }

    /**
     * Add geometry object
     *
     * @param Request $request
     * @return ErrorJsonResponse|GeoJsonResponse
     */
    public function addAction(Request $request)
    {
        $geoObjectsService = $this->get('geo.objects');

        try {
            $geometry = $geoObjectsService->createGeometry($request, $request->get('geometry_type'));

            $result = $geoObjectsService->addGeoobject(
                $geometry,
                $request->get('type'),
                $request->get('reference_id')
            );

        } catch (InvalidArgumentException $e) {
            return new ErrorJsonResponse($e->getMessage());

        } catch (DBALException $e) {
            return new ErrorJsonResponse($e->getMessage());
        }

        if (false === $result) {
            return new ErrorJsonResponse('Error!');
        }

        return new GeoJsonResponse($result);
    }

    /**
     * Update geometry object
     *
     * @param Request $request
     * @return ErrorJsonResponse|GeoJsonResponse
     */
    public function updateAction(Request $request)
    {
        $geoObjectsService = $this->get('geo.objects');

        try {
            $geometry = $geoObjectsService->createGeometry($request, $request->get('geometry_type'));

            $result = $geoObjectsService->updateGeoobject(
                $request->get('id'),
                $geometry,
                $request->get('type'),
                $request->get('reference_id')
            );

        } catch (InvalidArgumentException $e) {
            return new ErrorJsonResponse($e->getMessage());

        } catch (DBALException $e) {
            return new ErrorJsonResponse($e->getMessage());
        }

        if (false === $result) {
            return new ErrorJsonResponse('Error!');
        }

        return new GeoJsonResponse($result);

    }

    /**
     * Get geometry object by reference id
     *
     * @param Request $request
     * @return ErrorJsonResponse|GeoJsonResponse
     */
    public function byReferenceAction(Request $request)
    {
        $geoObjectsService = $this->get('geo.objects');

        try {
            $result = $geoObjectsService->getByReference($request->get('id'), $request->get('type'));

        } catch (InvalidArgumentException $e) {
            return new ErrorJsonResponse($e->getMessage());

        } catch (ObjectNotFoundException $e) {
            return new ErrorJsonResponse($e->getMessage());
        }

        return new GeoJsonResponse($result);
    }

    /**
     * Get geometry object by id
     *
     * @param Request $request
     * @return ErrorJsonResponse|GeoJsonResponse
     */
    public function byIdAction(Request $request)
    {
        $geoObjectsService = $this->get('geo.objects');

        try {
            $result = $geoObjectsService->getById($request->get('id'));

        } catch (InvalidArgumentException $e) {
            return new ErrorJsonResponse($e->getMessage());

        } catch (ObjectNotFoundException $e) {
            return new ErrorJsonResponse($e->getMessage());
        }

        return new GeoJsonResponse($result);
    }

    /**
     * Get geo objects types
     *
     * @param Request $request
     * @return GeoJsonResponse
     */
    public function getTypesAction(Request $request)
    {
        $geoInfoProvidersService = $this->get('geo.info.providers');

        $result = $geoInfoProvidersService->getAll();

        if (count($result) <= 0) {
            return new GeoJsonResponse([]);
        }

        return new GeoJsonResponse($result);
    }

    /**
     * Get type by name
     *
     * @param Request $request
     * @return ErrorJsonResponse|GeoJsonResponse
     */
    public function getTypeByNameAction(Request $request)
    {
        $geoInfoProvidersService = $this->get('geo.info.providers');

        $result = $geoInfoProvidersService->getProvider($request->get('name'));

        if (!$result) {
            return new ErrorJsonResponse('Type not found!');
        }

        return new GeoJsonResponse($result);
    }

    /**
     * @param Request $request
     * @return array|GeoJsonResponse
     */
    public function getObjectsAction(Request $request)
    {
        $geoService = $this->get('geo.objects');

        try {
            $result = $geoService->getAll(
                $request->get('type'),
                $request->get('limit', 20),
                $request->get('skip'),
                $request->get('geometry_type')
            );

        } catch (ObjectNotFoundException $e) {
            return new ErrorJsonResponse($e->getMessage());
        }

        return new GeoJsonResponse($result);
    }

    /**
     * @param Request $request
     * @return ErrorJsonResponse|GeoJsonResponse
     */
    public function getReferenceObjectsListAction(Request $request)
    {
        $geoObjectsService = $this->get('geo.objects');

        try {
            $result = $geoObjectsService->getReferenceObjectsList(
                $request->get('type'),
                $request->get('query'),
                $request->get('limit'),
                $request->get('skip')
            );

        } catch (\RuntimeException $e) {
            return new ErrorJsonResponse($e->getMessage());

        } catch (InfoProviderNotFoundException $e) {
            return new ErrorJsonResponse($e->getMessage());

        } catch (ObjectNotFoundException $e) {
            return new ErrorJsonResponse($e->getMessage());
        }

        return new GeoJsonResponse($result);
    }

    /**
     * @param Request $request
     * @return \CMS\GeoBundle\Response\ErrorJsonResponse|\CMS\GeoBundle\Response\GeoJsonResponse
     */
    public function getInfoAction(Request $request)
    {
        $geoObjectsService = $this->get('cms.geo.objects');

        try {
            $result = $geoObjectsService->getInfo($request->get('id'), $request->get('provider'));

        } catch (InvalidArgumentException $e) {
            return new ErrorJsonResponse($e->getMessage());

        } catch (\RuntimeException $e) {
            return new ErrorJsonResponse($e->getMessage());

        } catch (InfoProviderNotFoundException $e) {
            return new ErrorJsonResponse($e->getMessage());

        } catch (ObjectNotFoundException $e) {
            return new ErrorJsonResponse($e->getMessage());
        }

        return $result;
    }

    /**
     * @param Request $request
     * @return ErrorJsonResponse|GeoJsonResponse
     * @throws InvalidArgumentException
     */
    public function removeAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            return new ErrorJsonResponse('Access denied!');
        }

        $geoObjectsService = $this->get('geo.objects');

        try {
            $result = $geoObjectsService->removeGeoobject($request->get('id'));

        } catch (InvalidArgumentException $e) {
            return new ErrorJsonResponse($e->getMessage());

        } catch (\RuntimeException $e) {
            return new ErrorJsonResponse($e->getMessage());

        } catch (InfoProviderNotFoundException $e) {
            return new ErrorJsonResponse($e->getMessage());

        } catch (ObjectNotFoundException $e) {
            return new ErrorJsonResponse($e->getMessage());
        }

        return new GeoJsonResponse($result);
    }
}
