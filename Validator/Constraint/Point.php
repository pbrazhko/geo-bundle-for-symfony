<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 03.12.15
 * Time: 13:26
 */

namespace CMS\GeoBundle\Validator\Constraint;


use Symfony\Component\Validator\Constraint;

class Point extends Constraint
{
    public $message = 'Field %field% is not valid';

    /**
     * Returns the name of the class that validates this constraint.
     *
     * By default, this is the fully qualified name of the constraint class
     * suffixed with "Validator". You can override this method to change that
     * behaviour.
     *
     * @return string
     */
    public function validatedBy()
    {
        return 'CMS\GeoBundle\Validator\PointValidator';
    }
}