<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 03.12.15
 * Time: 13:25
 */

namespace CMS\GeoBundle\Validator;


use CMS\GeoBundle\Exceptions\InvalidArgumentException;
use CMS\GeoBundle\Types\Point;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class PointValidator extends ConstraintValidator
{
    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        try{
            $point = new Point();
            $point->fromString($value);
        }
        catch(InvalidArgumentException $e){
            $this->context->buildViolation('validation.geometry.format')
                ->setParameter('%string%', $value)
                ->addViolation();
        }
    }
}