<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 06.10.14
 * Time: 15:11
 */

namespace CMS\GeoBundle\Response;


class GeoObjectsListResponse implements \IteratorAggregate{

    private $objects = array();

    public function add($id, $name){
        if (!isset($this->objects[$id])) {
            $this->objects[$id] = $name;
        }

        return $this;
    }

    public function delete($id){
        if (isset($this->objects[$id])){
            unset($this->objects[$id]);
        }

        return $this;
    }

    public function getIterator(){
        return new \ArrayIterator($this->objects);
    }
} 