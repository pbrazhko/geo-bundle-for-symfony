<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 08.09.14
 * Time: 15:47
 */

namespace CMS\GeoBundle\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class ErrorJsonResponse extends JsonResponse{
    public function __construct($message){
        parent::__construct(array('status' => 'error', 'message' => $message), 400);
    }
} 