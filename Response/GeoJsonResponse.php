<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 08.09.14
 * Time: 16:56
 */

namespace CMS\GeoBundle\Response;

use CMS\GeoBundle\Normalizer\GeoObjectIconNormalizer;
use CMS\GeoBundle\Normalizer\GeoObjectsInfoNormalizer;
use CMS\GeoBundle\Normalizer\ObjectsNormalizer;
use CMS\GeoBundle\Normalizer\ORMResultNativeNormalizer;
use CMS\GeoBundle\Normalizer\ProviderNormalizer;
use CMS\GeoBundle\Normalizer\TypesNormalizer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

class GeoJsonResponse extends Response{
    public function __construct($data, $status = 200, $headers = array()){
        $serializer = $this->getSerializer();

        parent::__construct($serializer->serialize($data, 'json'), $status, $headers);
    }

    private function getSerializer(){
        return new Serializer(array(
                new ObjectsNormalizer(),
                new TypesNormalizer(),
                new ORMResultNativeNormalizer(),
                new ProviderNormalizer(),
                new GeoObjectIconNormalizer()
            ),
            array(new JsonEncoder()
            )
        );
    }
} 