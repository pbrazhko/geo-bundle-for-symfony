<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 27.10.14
 * Time: 14:24
 */

namespace CMS\GeoBundle\Response;


class GeoObjectIconResponse {

    /**
     * The symbol's fill color. All CSS3 colors are supported except for extended named colors.
     * For symbol markers, this defaults to 'black'. For symbols on polylines,
     * this defaults to the stroke color of the corresponding polyline.
     *
     * @var string
     */
    private $fillColor;

    /**
     * The symbol's fill opacity. Defaults to 0.
     *
     * @var int
     */
    private $fillOpacity;

    /**
     * The symbol's path, which is a built-in symbol path, or a custom path expressed using SVG path notation. Required.
     *
     * BACKWARD_CLOSED_ARROW
     * BACKWARD_OPEN_ARROW
     * CIRCLE
     * FORWARD_CLOSED_ARROW
     * FORWARD_OPEN_ARROW
     *
     * @var string
     */
    private $path;

    /**
     * The angle by which to rotate the symbol, expressed clockwise in degrees.
     * Defaults to 0. A symbol in an IconSequence where fixedRotation is false
     * is rotated relative to the angle of the edge on which it lies.
     *
     * @var int
     */
    private $rotation;

    /**
     * The amount by which the symbol is scaled in size. For symbol markers,
     * this defaults to 1; after scaling, the symbol may be of any size.
     * For symbols on a polyline, this defaults to the stroke weight of the polyline; after scaling,
     * the symbol must lie inside a square 22 pixels in size centered at the symbol's anchor.
     *
     * @var int
     */
    private $scale;

    /**
     * The symbol's stroke color. All CSS3 colors are supported except for extended named colors.
     * For symbol markers, this defaults to 'black'. For symbols on a polyline,
     * this defaults to the stroke color of the polyline.
     *
     * @var string
     */
    private $strokeColor;

    /**
     * The symbol's stroke opacity. For symbol markers, this defaults to 1. For symbols on a polyline,
     * this defaults to the stroke opacity of the polyline.
     *
     * @var int
     */
    private $strokeOpacity;

    /**
     * The symbol's stroke weight. Defaults to the scale of the symbol.
     *
     * @var int
     */
    private $strokeWeight;

    /**
     * @return mixed
     */
    public function getFillColor()
    {
        return $this->fillColor;
    }

    /**
     * @param mixed $fillColor
     * @return $this
     */
    public function setFillColor($fillColor)
    {
        $this->fillColor = $fillColor;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFillOpacity()
    {
        return $this->fillOpacity;
    }

    /**
     * @param mixed $fillOpacity
     * @return $this
     */
    public function setFillOpacity($fillOpacity)
    {
        $this->fillOpacity = $fillOpacity;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     * @return $this
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRotation()
    {
        return $this->rotation;
    }

    /**
     * @param mixed $rotation
     * @return $this
     */
    public function setRotation($rotation)
    {
        $this->rotation = $rotation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getScale()
    {
        return $this->scale;
    }

    /**
     * @param mixed $scale
     * @return $this
     */
    public function setScale($scale)
    {
        $this->scale = $scale;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStrokeColor()
    {
        return $this->strokeColor;
    }

    /**
     * @param mixed $strokeColor
     * @return $this
     */
    public function setStrokeColor($strokeColor)
    {
        $this->strokeColor = $strokeColor;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStrokeOpacity()
    {
        return $this->strokeOpacity;
    }

    /**
     * @param mixed $strokeOpacity
     * @return $this
     */
    public function setStrokeOpacity($strokeOpacity)
    {
        $this->strokeOpacity = $strokeOpacity;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStrokeWeight()
    {
        return $this->strokeWeight;
    }

    /**
     * @param mixed $strokeWeight
     * @return $this
     */
    public function setStrokeWeight($strokeWeight)
    {
        $this->strokeWeight = $strokeWeight;

        return $this;
    }
}