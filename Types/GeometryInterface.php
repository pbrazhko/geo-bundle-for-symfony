<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 04.09.14
 * Time: 13:13
 */

namespace CMS\GeoBundle\Types;


interface GeometryInterface
{
    const GEOMETRY_TYPE_POINT = 'POINT';
    const GEOMETRY_TYPE_POLYGON = 'POLYGON';
    const GEOMETRY_TYPE_MULTIPOLIGON = 'MULTIPOLYGON';

    /**
     * @return string
     */
    public function getGeometryType();

    public function getAttributes();

    /**
     * Return geometry as array
     *
     * @return array
     */
    public function toArray();

    /**
     * Return geometry as string
     *
     * @return string
     */
    public function toString();

    /**
     * Parse geo object from string
     *
     * @param $data
     * @return boolean
     */
    public function fromString($data);
} 