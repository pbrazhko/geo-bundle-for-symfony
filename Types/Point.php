<?php
namespace CMS\GeoBundle\Types;

use CMS\GeoBundle\Exceptions\InvalidArgumentException;

class Point extends AbstractGeometry
{
    /**
     * @return string
     */
    public function getGeometryType()
    {
        return self::GEOMETRY_TYPE_POINT;
    }

    public function getAttributes()
    {
        return [
            'geometry'
        ];
    }

    public function validate()
    {
        parent::validate();

        if (count($this->geometry) != 2) {
            throw new InvalidArgumentException('Point is not valid!');
        }
    }

    /**
     * Return geometry as array
     *
     * @return array
     */
    public function toArray()
    {
        return isset($this->data['geometry']) ? $this->data['geometry'] : [];
    }

    /**
     * Return geometry as string
     *
     * @return string
     */
    public function toString()
    {
        return $this->geometry[0] . ' ' . $this->geometry[1];
    }

    /**
     * Parse geo object from string
     *
     * @param $data
     * @throws \CMS\GeoBundle\Exceptions\InvalidArgumentException
     * @return boolean
     */
    public function fromString($data)
    {
        if (!preg_match('/^POINT\((.*)\)$/', $data, $matches)) {
            throw new InvalidArgumentException('Point is not valid!');
        }

        if (!preg_match('/^[0-9\.\s\,\-e]+$/', $matches[1])) {
            throw new InvalidArgumentException(sprintf('Point is not valid! Example: 12.01212 36.565656'));
        }

        $coords = @preg_split("/(\s+|\,\s?)+/", $matches[1]);

        if (count($coords) != 2) {
            throw new InvalidArgumentException('Point is not valid!');
        }

        $this->data['geometry'] = $coords;

        return true;
    }
}