<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 04.09.14
 * Time: 13:09
 */

namespace CMS\GeoBundle\Types;

use CMS\GeoBundle\Exceptions\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractGeometry implements GeometryInterface
{

    protected $data = array();

    public function __construct(Request $request = null){
        if (null !== $request){

            $attributes = $this->getAttributes();

            if (count($attributes) > 0){
                foreach($attributes as $attr){
                    $this->data[$attr] = $request->get($attr);
                }
            }

            $this->validate();
        }
    }

    public function validate(){

        $attributes = $this->getAttributes();

        if (count($attributes) > 0){
            foreach($attributes as $attr){

                $value = $this->{$attr};

                if (null === $value){
                    throw new InvalidArgumentException(sprintf('Variable "%s" is not defined', $attr));
                }

                $this->data[$attr] = $value;
            }
        }

        return true;
    }

    public function __get($name){
        if (isset($this->data[$name])){
            return $this->data[$name];
        }

        return null;
    }

    public function __toString()
    {
        return $this->toString();
    }
}