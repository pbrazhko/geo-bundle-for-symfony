<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 04.09.14
 * Time: 13:09
 */

namespace CMS\GeoBundle\Types;

use CMS\GeoBundle\Exceptions\InvalidArgumentException;

class Polygon extends AbstractGeometry
{
    public function getAttributes(){
        return [
            'geometry'
        ];
    }

    public function validate(){

        $geometry = $this->geometry;

        if (null === $geometry || !is_array($geometry) || count($geometry) != 1){
            throw new InvalidArgumentException(sprintf('Variable "%s" is not valid!', 'Polygon'));
        }

        foreach($geometry as $k=>$polygon){
            if (!is_array($polygon) || count($polygon) < 3){
                throw new InvalidArgumentException(sprintf('Count points in "%s" is no valid', 'Polygon'));
            }

            foreach($polygon as $point){
                if (!is_array($point) || count($point) != 2){
                    throw new InvalidArgumentException(sprintf('Variable "%s" is not valid!', 'Polygon'));
                }
            }

            $firstPoint = reset($polygon);
            $lostPoint  = array_pop($polygon);

            if ($firstPoint[0] != $lostPoint[0] || $firstPoint[1] != $lostPoint[1]){
                array_push($geometry[$k], $firstPoint);
            }
        }

        $this->data['geometry'] = $geometry;

        return true;
    }

    /**
     * Parse geo object from string
     *
     * @param $data
     * @throws \CMS\GeoBundle\Exceptions\InvalidArgumentException
     * @return boolean
     */
    public function fromString($data)
    {
        if (!preg_match('/^POLYGON\(\(([\s0-9\,\-\.]+)\)\)$/', $data, $matches)){
            throw new InvalidArgumentException('Polygon is not valid!');
        }

        $linestring = $matches[1];

        $polygons = preg_split('/\)\,\(/', $linestring);

        if (count($polygons) < 1){
            throw new InvalidArgumentException('Polygon is not valid!');
        }

        foreach($polygons as $kpolygons=>$polygon){
            $this->data['geometry'][$kpolygons] = array();

            $points = preg_split('/\,/', $polygon);

            if (count($points) < 1){
                throw new InvalidArgumentException('Polygon is not valid! Example: 12.01212 36.565656');
            }

            foreach($points as $kpoins => $point){
                if (!preg_match('/([0-9\.\-]+)\s+([0-9\.\-]+)/', $point, $matches)){
                    throw new InvalidArgumentException(sprintf('Point (index: %s) in polygon is not valid', $kpoins));
                }

                $this->data['geometry'][$kpolygons][$kpoins] = [(float) $matches[1], (float) $matches[2]];
            }
        }

        return true;
    }

    /**
     * Return geometry as array
     *
     * @return array
     */
    public function toArray()
    {
        return isset($this->data['geometry'])?$this->data['geometry']:array();
    }

    /**
     * Return geometry as string
     *
     * @return string
     */
    public function toString()
    {
        $polygon = '';

        if (count($this->geometry) > 0){
            $polygon .= 'POLYGON(';
            foreach($this->geometry as $kpolygon => $_polygon){
                if (count($_polygon) > 0){
                    $polygon .= "(";
                    foreach($_polygon as $kpoint => $point){
                        list($lat, $lon) = $point;

                        $polygon .= $lat . " " .$lon;

                        if ((count($_polygon)-1) > $kpoint){
                            $polygon .= ", ";
                        }
                    }
                    $polygon .= ")";

                    if ((count($this->geometry)-1) > $kpolygon){
                        $polygon .= ", ";
                    }
                }
            }
            $polygon .= ')';
        }

        return $polygon;
    }
}