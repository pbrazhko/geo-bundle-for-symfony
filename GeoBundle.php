<?php

namespace CMS\GeoBundle;

use CMS\GeoBundle\Compiler\GeoObjectsInfoCompiler;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class GeoBundle extends Bundle
{
    public function build(ContainerBuilder $container){
        parent::build($container);

        $container->addCompilerPass(new GeoObjectsInfoCompiler());
    }
}
